import { override, extend} from 'flarum/extend';
import app from 'flarum/app';
import DiscussionListItem from 'flarum/components/DiscussionListItem';
import DiscussionList from 'flarum/components/DiscussionList';
import Discussion from 'flarum/models/Discussion';
import DiscussionPage from 'flarum/components/DiscussionPage';
import Page from 'flarum/components/Page';
import Model from 'flarum/Model';
import StripeUsers from './../common/models/StripeUsers';
import TagPlan from './../common/models/TagPlan';
import PayPage from './components/PayPage';


app.initializers.add('cmc-paywall', () => {

    app.store.models['stripeusers'] = StripeUsers;
    app.store.models['tag_plan'] = TagPlan;
    app.routes.subscribe = {path: '/subscribe', component: PayPage.component()};

    Discussion.prototype.isPremium = Model.attribute('isPremium');
    Discussion.prototype.canSeePremiumContent = Model.attribute('canSeePremiumContent');

    extend(Page.prototype, 'view', function(asd) {
        console.log('test');
        console.log(this);
    });

    extend(DiscussionPage.prototype, 'view', function(asd) {
        // Note:  even if you try to hack/change this script, you will still
        // just receive an empty discussion page.  Just let it redirect kthx
        
 
        let discussion = this.discussion;
    
        
        if ( (discussion) && discussion.isPremium() && !discussion.canSeePremiumContent()) {
            

           
            // Replace the pages view with a redirect message before calling m.route()
            asd.children = m("div", {"style":{"text-align":"center"}},
                [
                    m("h2", 
                    "Oops! That's Premium Content!"
                    ),
                    m("p", 
                    "One moment..."
                    )
                ]
                );

                 asd.children = <PayPage />;


            //console.log(asd);
            //m.route(app.route('subscribe')); 
            return asd;
            
            //console.log('redirect NOW!');
        }

       
        //console.log('isPremium: ' + this.props.discussion.isPremium());
        //return original();
    });

  })
