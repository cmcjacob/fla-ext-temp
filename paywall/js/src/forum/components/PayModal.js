import Modal from 'flarum/components/Modal';

/**
 * The `EditPageModal` component shows a modal dialog which allows the user
 * to create or edit a page.
 */
var cbhtml = `<button
  class="Button Button--primary Button--block"
  id="checkout-button"
  role="link"
  title="Checkout"
>
  Checkout with Stripes
</button>`;

var stripe, plan;

export default class PayModal extends Modal {
    init() {
        super.init();

        stripe = this.props.stripe;
        plan = this.props.plan;
        console.log(this.props.stripe);
        console.log('inherited stripe ^^^');

        
    }

    config(isInitialized) {
        console.log('config');
        console.log(isInitialized);
        console.log(stripe);
        //if (!isInitialized) return;

        if (! stripe) return;
      
        var checkoutButton = document.getElementById('checkout-button');
        console.log(checkoutButton);    


        checkoutButton.addEventListener('click', function() {

            console.log('clicked');
        // When the customer clicks on the button, redirect
        // them to Checkout.
        stripe.redirectToCheckout({
            items: [{plan: plan, quantity: 1}],

            // Do not rely on the redirect to the successUrl for fulfilling
            // purchases, customers may not always reach the success_url after
            // a successful payment.
            // Instead use one of the strategies described in
            // https://stripe.com/docs/payments/checkout/fulfillment
            successUrl: 'https://localhost:443/api/stripe/success',
            cancelUrl: 'https://localhost:443/api/stripe/cancel',
        })
        
        .then(function (result) {
            if (result.error) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer.
                var displayError = document.getElementById('error-message');
                displayError.textContent = result.error.message;
                console.log(result);
            }
        });
    });
}

    
    className() {
        return 'LogInModal Modal--small';
    }

    title() {
        console.log('tile called');
        const title = this.props.title;
        return title ? title : 'Purchase Premium Subscription';
    }

 

    content() {
        console.log('content called');
       

        return m('div.Modal-body', m.trust(this.props.content));
    }


    onhide() {
       // m.route(app.route('paywall'));
    }

    

}