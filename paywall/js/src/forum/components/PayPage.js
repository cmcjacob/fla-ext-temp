

import app from 'flarum/app';
import Page from 'flarum/Component';
import PayModal from './PayModal';

const checkoutButtonHtml = `<button
class="Button Button--primary Button--block"
id="checkout-button"
role="link"
title="Checkout"
>
Checkout with Stripe
</button>`;

var stripe;
var subscribeSettings, payPage;

export default class PayPage extends Page {

    config(isInitialized) {
        console.log(isInitialized);
        if (!isInitialized) return;
        
      
        // Create a Stripe client.
        stripe = Stripe(subscribeSettings[1]);
        console.log(stripe);
        this.stripe = stripe;
        var checkoutButton = document.getElementById('checkout-button');

        
       
        
// Create an instance of Elements.


// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};


var plan;
plan = subscribeSettings[0];

/*checkoutButton.addEventListener('click', function() {
  // When the customer clicks on the button, redirect
  // them to Checkout.
  stripe.redirectToCheckout({
    items: [{plan: plan, quantity: 1}],

    // Do not rely on the redirect to the successUrl for fulfilling
    // purchases, customers may not always reach the success_url after
    // a successful payment.
    // Instead use one of the strategies described in
    // https://stripe.com/docs/payments/checkout/fulfillment
    successUrl: 'https://localhost.com/api/stripe/success',
    cancelUrl: 'https://localhost.com/api/stripe/cancel',
  })
  
  .then(function (result) {
    if (result.error) {
      
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer.
      var displayError = document.getElementById('error-message');
      displayError.textContent = result.error.message;
      console.log(result);
    }
  });

}) ;
*/

}

   
    init() {
      subscribeSettings = app.forum.attribute("cmc.paywall.subscribe");
      payPage = app.forum.attribute("cmc.paywall.page");
      payPage[1] = payPage[1].replace('[checkout]', checkoutButtonHtml);

      // Create a Stripe client.
      stripe = Stripe(subscribeSettings[1]);

      this.displayOnce = false;
    }


    view() { 
      if (stripe && !this.displayOnce) {
        this.displayOnce = true;
        app.modal.show(new PayModal({stripe: stripe, title: payPage[0], content: payPage[1], plan: subscribeSettings[0]}));
        return(<div>RESTRICTED</div>);
      } else {
        return (<div className="container"><h2>Oops...</h2>The content you are trying to access is restricted to premium subscribers. If you are having problems purchasing a subscription, or if you are already subscribed, please contact the forum administrator.</div>);   
    }
  }
}


