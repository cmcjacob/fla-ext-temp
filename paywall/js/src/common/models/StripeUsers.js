import app from 'flarum/app';
import Model from 'flarum/Model';
import mixin from 'flarum/utils/mixin';
import computed from 'flarum/utils/computed';

export default class StripeUsers extends mixin(Model, {
    stripe_id: Model.attribute('stripe_id'),
    user_id: Model.attribute('user_id'),
    card_brand: Model.attribute('card_brand'),
    card_last_four: Model.attribute('card_last_four'),
    trial_ends_at: Model.attribute('trial_ends_at', Model.transformDate)
   
}) {
    
}