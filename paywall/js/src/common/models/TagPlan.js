import app from 'flarum/app';
import Model from 'flarum/Model';
import mixin from 'flarum/utils/mixin';
import computed from 'flarum/utils/computed';

export default class TagPlan extends mixin(Model, {
    tag_id: Model.attribute('tag_id'),
    stripe_plan: Model.attribute('stripe_plan')
}) {
    
}