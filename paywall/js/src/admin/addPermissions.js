import {extend} from 'flarum/extend';
import app from 'flarum/app';
import ItemList from 'flarum/utils/ItemList';
import PermissionGrid from 'flarum/components/PermissionGrid';

export default function () {
    extend(PermissionGrid.prototype, 'permissionItems', groups => {
        const items = new ItemList();

        items.add('see-premium-content', {
            icon: 'fas fa-eye',
            label:'View premium discussions',
            permission: 'cmc.paywall.view-premium-post',
             allowGuest: false,
        });

        items.add('post-premium-content', {
            icon: 'fas fa-edit',
            label:'Create premium discussions',
            permission: 'cmc.paywall.create-premium-post',
             allowGuest: false,
        });
       
       
        groups.add('cmc-paywall', {
            label: 'Paywall',
            children: items.toArray(),
        });
    });
}