import {extend} from 'flarum/extend';
import app from 'flarum/app';
import addPermissions from './addPermissions';
import StripeUsers from './../common/models/StripeUsers';
import TagPlan from './../common/models/TagPlan';

app.initializers.add('cmc-paywall', () => {
  addPermissions();
  app.store.models['stripeusers'] = StripeUsers;
  app.store.models['tag_plan'] = TagPlan;

});

import AdminNav from 'flarum/components/AdminNav';
import AdminLinkButton from 'flarum/components/AdminLinkButton';
import PaywallPage from './components/PaywallPage';


    app.routes.paywall = { path: '/paywall', component: PaywallPage.component() };
    app.extensionSettings['cmc-paywall'] = () => m.route(app.route('paywall'));

    extend(AdminNav.prototype, 'items', items => {
        items.add('paywall', AdminLinkButton.component({
          href: app.route('paywall'),
          icon: 'fas fa-bars',
          children: 'Paywall',
          description: 'Add a paywall using Braintree SDK to Flarum tags/groups',
        }));
      });
