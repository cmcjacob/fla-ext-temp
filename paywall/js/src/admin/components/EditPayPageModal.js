import Modal from 'flarum/components/Modal';
import Button from 'flarum/components/Button';
import { slug } from 'flarum/utils/string';
import saveSettings from "flarum/utils/saveSettings";

/**
 * The `EditPageModal` component shows a modal dialog which allows the user
 * to create or edit a page.
 */
export default class EditPayPageModal extends Modal {
    init() {
        super.init();

        this.pageTitle = m.prop(app.data.settings['cmc.paywall.pay-page-title'] || 'Purchase Premium Subscription');
        this.pageContent =m.prop(app.data.settings['cmc.paywall.pay-page-content'] || '')
        this.isHtml  = m.prop(app.data.settings['cmc.paywall.pay-page-has-html'] > 0);
    }

    className() {
        return 'EditPageModal Modal--large';
    }

    title() {
        const title = this.pageTitle();
        return title ? '[Edit] ' + title : '[Edit] Purchase Premium Subscription';
    }

    content() {
        return (
            <div className="Modal-body">
                <div className="Form">
                    <div className="Form-group">
                        <label>Page Title</label>
                        <input
                            className="FormControl"
                            placeholder='Add a title'
                            value={this.pageTitle()}
                            onchange={m.withAttr('value', this.updateSetting.bind(this,
                              this.pageTitle, 'cmc.paywall.pay-page-title'))}
                        />
                    </div>

                  

                    <div className="Form-group">
                        <label>Page Content</label>
                        <textarea
                            className="FormControl"
                            rows="5"
                            value={this.pageContent()}
                            onchange={m.withAttr('value', this.updateSetting.bind(this, 
                              this.pageContent, 'cmc.paywall.pay-page-content'))}
                            placeholder='Add page content'
                        />
                    </div>

                    <label>By default, the <b>Checkout</b> button will appear after the content.<br/>
                    You can change its location in the content using <b>[checkout]</b> tag in the box above code. </label>
<br/>

                    <div className="Form-group">
                        <div>
                            <label className="checkbox">
                                <input type="checkbox" value="1" checked={this.isHtml()} 
                                 onchange={m.withAttr('checked', this.updateSetting.bind(this, this.isHtml, 'cmc.paywall.pay-page-has-html'))}/>
                                Contains HTML
                            </label>
                        </div>
                    </div>
                    <div className="Form-group">
                        {Button.component({
                            type: 'submit',
                            className: 'Button Button--primary EditPageModal-save',
                            loading: this.loading,
                            children: 'Save Page'
                        })}
                       
                        
                    </div>
                </div>
            </div>
        );
    }

    onsubmit(e) {
        e.preventDefault();

        this.loading = true;
        this.hide.bind(this);

    }

    onhide() {
        m.route(app.route('paywall'));
    }

    

    

    updateSetting(prop, setting, value) {
        saveSettings({
            [setting]: value
        });

        prop(value)
    }

}