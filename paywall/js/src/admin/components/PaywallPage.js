import app from 'flarum/app';
import saveSettings from "flarum/utils/saveSettings";
import Page from 'flarum/Component';
import Select from 'flarum/components/Select';
import Switch from 'flarum/components/Switch';
import Button from 'flarum/components/Button';
import PermissionDropdown from 'flarum/components/PermissionDropdown';
import EditPayPageModal from './EditPayPageModal';

var stripe;

export default class PaywallPage extends Page {


    config(isInitialized) {
        console.log(isInitialized);
        if (!isInitialized) return;
        // Create a Stripe client.
        //stripe = Stripe('pk_test_F1KdZCT3xBGhWOw7WaZj2B1m00dLALMX3c');
        var checkoutButton = document.getElementById('checkout-button-plan_FnK8CnCgkxuctC');
        //console.log(Stripe('pk_test_F1KdZCT3xBGhWOw7WaZj2B1m00dLALMX3c'));
        console.log(checkoutButton);
        console.log(stripe);
        
// Create an instance of Elements.


// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

    // Create an instance of the card Element.
   // var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    //card.mount('#card-element');

    //var stripe = Stripe('pk_live_LnHGuG2THrZrRUCAH959cOYV009WYclXeW');




checkoutButton.addEventListener('click', function () {
  // When the customer clicks on the button, redirect
  // them to Checkout.
  stripe.redirectToCheckout({
    items: [{plan: 'plan_FnK8CnCgkxuctC', quantity: 1}],

    // Do not rely on the redirect to the successUrl for fulfilling
    // purchases, customers may not always reach the success_url after
    // a successful payment.
    // Instead use one of the strategies described in
    // https://stripe.com/docs/payments/checkout/fulfillment
    successUrl: 'https://localhost.com/api/stripe/success',
    cancelUrl: 'https://localhost.com/api/stripe/cancel',
  })
  
  .then(function (result) {
    if (result.error) {
      
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer.
      var displayError = document.getElementById('error-message');
      displayError.textContent = result.error.message;
      console.log(result);
    }
  });
});

}

  
    init() {
        this.testSecretKey = m.prop(app.data.settings['cmc.paywall.test-secret-key'] || '');
        this.testPublishableKey = m.prop(app.data.settings['cmc.paywall.test-publishable-key'] || '');
        this.liveSecretKey = m.prop(app.data.settings['cmc.paywall.live-secret-key'] || '');
        this.livePublishableKey = m.prop(app.data.settings['cmc.paywall.live-publishable-key'] || '');
        this.stripePlanID = m.prop(app.data.settings['cmc.paywall.stripe-plan-id'] || '');
        this.liveMode = m.prop(app.data.settings['cmc.paywall.live-mode'] > 0);
    }


    permissionDropdown() {
      return PermissionDropdown.component({
        permission: 'viewPremiumContent',
        allowGuest: false
      });
    }

    view() { 
        return (

           
    

            <div className="MailPage">
            <div className="container">
     
            <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
                <h2>Select Premium Groups</h2>
                <div className="helpText">
                Configure premium groups in Permissions settings.
                </div>

<form onsubmit={this.onsubmit.bind(this)}><br/>
                <h2>Stripe Settings</h2>

                
                {Switch.component({
                    state: this.liveMode(),
                    children: 'Use Live Keys in production',
                    onchange: this.updateSetting.bind(this, this.liveMode, 'cmc.paywall.live-mode')
                })}<br/>
               
<div className="MailPage-MailSettings">

<fieldset>
                <legend>Live API keys 
           
</legend>
                <ul>
                  <li class>
                    <div className="MailPage-MailSettings-input">
                      <label>Secret Key</label>
                        <input
                          className="FormControl"
                          type="password" placeholder="Live Secret Key" 
                          value={this.liveSecretKey()} 
                          onchange={m.withAttr('value', this.updateSetting.bind(this, this.liveSecretKey,'cmc.paywall.live-secret-key'))}/>
                    </div>
                  </li>
                  <li class>
                    <div className="MailPage-MailSettings-input">
                      <label>Publishable Key</label>
                      <input className="FormControl"
                        type="text" placeholder="Live Publishable Key" 
                        value={this.livePublishableKey()} 
                        onchange={m.withAttr('value', this.updateSetting.bind(this, this.livePublishableKey,'cmc.paywall.live-publishable-key'))}/>
                      </div>
                    </li>
                </ul>
                </fieldset>
                <fieldset>
                <legend>Test API keys</legend>
                <ul>
                  <li class>
                    <div className="MailPage-MailSettings-input">
                      <label>Secret Key</label>
                        <input
                          className="FormControl"
                          type="password" placeholder="Test Secret Key" 
                          value={this.testSecretKey()} 
                          onchange={m.withAttr('value', this.updateSetting.bind(this, this.testSecretKey,'cmc.paywall.test-secret-key'))}/>
                    </div>
                  </li>
                  <li class>
                    <div className="MailPage-MailSettings-input">
                      <label>Publishable Key</label>
                      <input className="FormControl"
                        type="text" placeholder="Test Publishable Key" 
                        value={this.testPublishableKey()} 
                        onchange={m.withAttr('value', this.updateSetting.bind(this, this.testPublishableKey,'cmc.paywall.test-publishable-key'))}/>
                      </div>
                    </li>
                </ul>
                </fieldset>
                

                
                </div>


                    <legend>Stripe Plan ID</legend>
                <div className="helpText">
                    Configure plans/pricing in the <b>Stripe Dashboard</b> under a Product.  All user subscriptions will be created to this product plan.
                </div>
                <input className="FormControl"
                        type="text" placeholder="Test Publishable Key" 
                        value={this.stripePlanID()} 
                        onchange={m.withAttr('value', this.updateSetting.bind(this, this.stripePlanID, 'cmc.paywall.stripe-plan-id'))}/>
                    
               

                
            </form>
           

   
    <div id="card-errors" role="alert"></div>
  
<h2>Test Express Checkout</h2>
<button
  style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em"
  id="checkout-button-plan_FnK8CnCgkxuctC"
  role="link"
>
  Checkout
</button>
<br/>

<h2>Configure Paywall Redirect Page</h2>
<div className="helpText">Customize what unsubscribed users will see when trying to access restricted content. </div>
{Button.component({
                            className: 'Button Button--primary',
                            icon: 'fas fa-plus',
                            children: 'Configure Pay Page',
                            onclick: () => app.modal.show(new EditPayPageModal()),
                        })}



<div id="error-message"></div>


            </div>
        </div>
    );

}
    
onsubmit(e) {
    e.preventDefault();


    this.loading = true;

   
}




updateSetting(prop, setting, value) {
    saveSettings({
        [setting]: value
    });

    prop(value)
}
}