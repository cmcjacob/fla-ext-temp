module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./admin.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./admin.js":
/*!******************!*\
  !*** ./admin.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_admin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/admin */ "./src/admin/index.js");
/* empty/unused harmony star reexport */

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _inheritsLoose; });
function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  subClass.__proto__ = superClass;
}

/***/ }),

/***/ "./src/admin/addPermissions.js":
/*!*************************************!*\
  !*** ./src/admin/addPermissions.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var flarum_extend__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flarum/extend */ "flarum/extend");
/* harmony import */ var flarum_extend__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(flarum_extend__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/app */ "flarum/app");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_utils_ItemList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/utils/ItemList */ "flarum/utils/ItemList");
/* harmony import */ var flarum_utils_ItemList__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_ItemList__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_components_PermissionGrid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/components/PermissionGrid */ "flarum/components/PermissionGrid");
/* harmony import */ var flarum_components_PermissionGrid__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_components_PermissionGrid__WEBPACK_IMPORTED_MODULE_3__);




/* harmony default export */ __webpack_exports__["default"] = (function () {
  Object(flarum_extend__WEBPACK_IMPORTED_MODULE_0__["extend"])(flarum_components_PermissionGrid__WEBPACK_IMPORTED_MODULE_3___default.a.prototype, 'permissionItems', function (groups) {
    var items = new flarum_utils_ItemList__WEBPACK_IMPORTED_MODULE_2___default.a();
    items.add('see-premium-content', {
      icon: 'fas fa-eye',
      label: 'View premium discussions',
      permission: 'cmc.paywall.view-premium-post',
      allowGuest: false
    });
    items.add('post-premium-content', {
      icon: 'fas fa-edit',
      label: 'Create premium discussions',
      permission: 'cmc.paywall.create-premium-post',
      allowGuest: false
    });
    groups.add('cmc-paywall', {
      label: 'Paywall',
      children: items.toArray()
    });
  });
});

/***/ }),

/***/ "./src/admin/components/EditPayPageModal.js":
/*!**************************************************!*\
  !*** ./src/admin/components/EditPayPageModal.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EditPayPageModal; });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_components_Modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/components/Modal */ "flarum/components/Modal");
/* harmony import */ var flarum_components_Modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_components_Modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_components_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/components/Button */ "flarum/components/Button");
/* harmony import */ var flarum_components_Button__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_components_Button__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_utils_string__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/utils/string */ "flarum/utils/string");
/* harmony import */ var flarum_utils_string__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_string__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! flarum/utils/saveSettings */ "flarum/utils/saveSettings");
/* harmony import */ var flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_4__);





/**
 * The `EditPageModal` component shows a modal dialog which allows the user
 * to create or edit a page.
 */

var EditPayPageModal =
/*#__PURE__*/
function (_Modal) {
  Object(_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(EditPayPageModal, _Modal);

  function EditPayPageModal() {
    return _Modal.apply(this, arguments) || this;
  }

  var _proto = EditPayPageModal.prototype;

  _proto.init = function init() {
    _Modal.prototype.init.call(this);

    this.pageTitle = m.prop(app.data.settings['cmc.paywall.pay-page-title'] || 'Purchase Premium Subscription');
    this.pageContent = m.prop(app.data.settings['cmc.paywall.pay-page-content'] || '');
    this.isHtml = m.prop(app.data.settings['cmc.paywall.pay-page-has-html'] > 0);
  };

  _proto.className = function className() {
    return 'EditPageModal Modal--large';
  };

  _proto.title = function title() {
    var title = this.pageTitle();
    return title ? '[Edit] ' + title : '[Edit] Purchase Premium Subscription';
  };

  _proto.content = function content() {
    return m("div", {
      className: "Modal-body"
    }, m("div", {
      className: "Form"
    }, m("div", {
      className: "Form-group"
    }, m("label", null, "Page Title"), m("input", {
      className: "FormControl",
      placeholder: "Add a title",
      value: this.pageTitle(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.pageTitle, 'cmc.paywall.pay-page-title'))
    })), m("div", {
      className: "Form-group"
    }, m("label", null, "Page Content"), m("textarea", {
      className: "FormControl",
      rows: "5",
      value: this.pageContent(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.pageContent, 'cmc.paywall.pay-page-content')),
      placeholder: "Add page content"
    })), m("label", null, "By default, the ", m("b", null, "Checkout"), " button will appear after the content.", m("br", null), "You can change its location in the content using ", m("b", null, "[checkout]"), " tag in the box above code. "), m("br", null), m("div", {
      className: "Form-group"
    }, m("div", null, m("label", {
      className: "checkbox"
    }, m("input", {
      type: "checkbox",
      value: "1",
      checked: this.isHtml(),
      onchange: m.withAttr('checked', this.updateSetting.bind(this, this.isHtml, 'cmc.paywall.pay-page-has-html'))
    }), "Contains HTML"))), m("div", {
      className: "Form-group"
    }, flarum_components_Button__WEBPACK_IMPORTED_MODULE_2___default.a.component({
      type: 'submit',
      className: 'Button Button--primary EditPageModal-save',
      loading: this.loading,
      children: 'Save Page'
    }))));
  };

  _proto.onsubmit = function onsubmit(e) {
    e.preventDefault();
    this.loading = true;
    this.hide.bind(this);
  };

  _proto.onhide = function onhide() {
    m.route(app.route('paywall'));
  };

  _proto.updateSetting = function updateSetting(prop, setting, value) {
    var _saveSettings;

    flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_4___default()((_saveSettings = {}, _saveSettings[setting] = value, _saveSettings));
    prop(value);
  };

  return EditPayPageModal;
}(flarum_components_Modal__WEBPACK_IMPORTED_MODULE_1___default.a);



/***/ }),

/***/ "./src/admin/components/PaywallPage.js":
/*!*********************************************!*\
  !*** ./src/admin/components/PaywallPage.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PaywallPage; });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/app */ "flarum/app");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/utils/saveSettings */ "flarum/utils/saveSettings");
/* harmony import */ var flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_Component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/Component */ "flarum/Component");
/* harmony import */ var flarum_Component__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_Component__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var flarum_components_Select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! flarum/components/Select */ "flarum/components/Select");
/* harmony import */ var flarum_components_Select__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(flarum_components_Select__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var flarum_components_Switch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! flarum/components/Switch */ "flarum/components/Switch");
/* harmony import */ var flarum_components_Switch__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(flarum_components_Switch__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var flarum_components_Button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! flarum/components/Button */ "flarum/components/Button");
/* harmony import */ var flarum_components_Button__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(flarum_components_Button__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var flarum_components_PermissionDropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! flarum/components/PermissionDropdown */ "flarum/components/PermissionDropdown");
/* harmony import */ var flarum_components_PermissionDropdown__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(flarum_components_PermissionDropdown__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _EditPayPageModal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./EditPayPageModal */ "./src/admin/components/EditPayPageModal.js");









var stripe;

var PaywallPage =
/*#__PURE__*/
function (_Page) {
  Object(_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(PaywallPage, _Page);

  function PaywallPage() {
    return _Page.apply(this, arguments) || this;
  }

  var _proto = PaywallPage.prototype;

  _proto.config = function config(isInitialized) {
    console.log(isInitialized);
    if (!isInitialized) return; // Create a Stripe client.
    //stripe = Stripe('pk_test_F1KdZCT3xBGhWOw7WaZj2B1m00dLALMX3c');

    var checkoutButton = document.getElementById('checkout-button-plan_FnK8CnCgkxuctC'); //console.log(Stripe('pk_test_F1KdZCT3xBGhWOw7WaZj2B1m00dLALMX3c'));

    console.log(checkoutButton);
    console.log(stripe); // Create an instance of Elements.
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)

    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    }; // Create an instance of the card Element.
    // var card = elements.create('card', {style: style});
    // Add an instance of the card Element into the `card-element` <div>.
    //card.mount('#card-element');
    //var stripe = Stripe('pk_live_LnHGuG2THrZrRUCAH959cOYV009WYclXeW');

    checkoutButton.addEventListener('click', function () {
      // When the customer clicks on the button, redirect
      // them to Checkout.
      stripe.redirectToCheckout({
        items: [{
          plan: 'plan_FnK8CnCgkxuctC',
          quantity: 1
        }],
        // Do not rely on the redirect to the successUrl for fulfilling
        // purchases, customers may not always reach the success_url after
        // a successful payment.
        // Instead use one of the strategies described in
        // https://stripe.com/docs/payments/checkout/fulfillment
        successUrl: 'https://localhost.com/api/stripe/success',
        cancelUrl: 'https://localhost.com/api/stripe/cancel'
      }).then(function (result) {
        if (result.error) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer.
          var displayError = document.getElementById('error-message');
          displayError.textContent = result.error.message;
          console.log(result);
        }
      });
    });
  };

  _proto.init = function init() {
    this.testSecretKey = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.test-secret-key'] || '');
    this.testPublishableKey = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.test-publishable-key'] || '');
    this.liveSecretKey = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.live-secret-key'] || '');
    this.livePublishableKey = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.live-publishable-key'] || '');
    this.stripePlanID = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.stripe-plan-id'] || '');
    this.liveMode = m.prop(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.data.settings['cmc.paywall.live-mode'] > 0);
  };

  _proto.permissionDropdown = function permissionDropdown() {
    return flarum_components_PermissionDropdown__WEBPACK_IMPORTED_MODULE_7___default.a.component({
      permission: 'viewPremiumContent',
      allowGuest: false
    });
  };

  _proto.view = function view() {
    return m("div", {
      className: "MailPage"
    }, m("div", {
      className: "container"
    }, m("script", {
      type: "text/javascript",
      src: "https://js.stripe.com/v2/"
    }), m("h2", null, "Select Premium Groups"), m("div", {
      className: "helpText"
    }, "Configure premium groups in Permissions settings."), m("form", {
      onsubmit: this.onsubmit.bind(this)
    }, m("br", null), m("h2", null, "Stripe Settings"), flarum_components_Switch__WEBPACK_IMPORTED_MODULE_5___default.a.component({
      state: this.liveMode(),
      children: 'Use Live Keys in production',
      onchange: this.updateSetting.bind(this, this.liveMode, 'cmc.paywall.live-mode')
    }), m("br", null), m("div", {
      className: "MailPage-MailSettings"
    }, m("fieldset", null, m("legend", null, "Live API keys"), m("ul", null, m("li", {
      "class": true
    }, m("div", {
      className: "MailPage-MailSettings-input"
    }, m("label", null, "Secret Key"), m("input", {
      className: "FormControl",
      type: "password",
      placeholder: "Live Secret Key",
      value: this.liveSecretKey(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.liveSecretKey, 'cmc.paywall.live-secret-key'))
    }))), m("li", {
      "class": true
    }, m("div", {
      className: "MailPage-MailSettings-input"
    }, m("label", null, "Publishable Key"), m("input", {
      className: "FormControl",
      type: "text",
      placeholder: "Live Publishable Key",
      value: this.livePublishableKey(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.livePublishableKey, 'cmc.paywall.live-publishable-key'))
    }))))), m("fieldset", null, m("legend", null, "Test API keys"), m("ul", null, m("li", {
      "class": true
    }, m("div", {
      className: "MailPage-MailSettings-input"
    }, m("label", null, "Secret Key"), m("input", {
      className: "FormControl",
      type: "password",
      placeholder: "Test Secret Key",
      value: this.testSecretKey(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.testSecretKey, 'cmc.paywall.test-secret-key'))
    }))), m("li", {
      "class": true
    }, m("div", {
      className: "MailPage-MailSettings-input"
    }, m("label", null, "Publishable Key"), m("input", {
      className: "FormControl",
      type: "text",
      placeholder: "Test Publishable Key",
      value: this.testPublishableKey(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.testPublishableKey, 'cmc.paywall.test-publishable-key'))
    })))))), m("legend", null, "Stripe Plan ID"), m("div", {
      className: "helpText"
    }, "Configure plans/pricing in the ", m("b", null, "Stripe Dashboard"), " under a Product.  All user subscriptions will be created to this product plan."), m("input", {
      className: "FormControl",
      type: "text",
      placeholder: "Test Publishable Key",
      value: this.stripePlanID(),
      onchange: m.withAttr('value', this.updateSetting.bind(this, this.stripePlanID, 'cmc.paywall.stripe-plan-id'))
    })), m("div", {
      id: "card-errors",
      role: "alert"
    }), m("h2", null, "Test Express Checkout"), m("button", {
      style: "background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em",
      id: "checkout-button-plan_FnK8CnCgkxuctC",
      role: "link"
    }, "Checkout"), m("br", null), m("h2", null, "Configure Paywall Redirect Page"), m("div", {
      className: "helpText"
    }, "Customize what unsubscribed users will see when trying to access restricted content. "), flarum_components_Button__WEBPACK_IMPORTED_MODULE_6___default.a.component({
      className: 'Button Button--primary',
      icon: 'fas fa-plus',
      children: 'Configure Pay Page',
      onclick: function onclick() {
        return flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.modal.show(new _EditPayPageModal__WEBPACK_IMPORTED_MODULE_8__["default"]());
      }
    }), m("div", {
      id: "error-message"
    })));
  };

  _proto.onsubmit = function onsubmit(e) {
    e.preventDefault();
    this.loading = true;
  };

  _proto.updateSetting = function updateSetting(prop, setting, value) {
    var _saveSettings;

    flarum_utils_saveSettings__WEBPACK_IMPORTED_MODULE_2___default()((_saveSettings = {}, _saveSettings[setting] = value, _saveSettings));
    prop(value);
  };

  return PaywallPage;
}(flarum_Component__WEBPACK_IMPORTED_MODULE_3___default.a);



/***/ }),

/***/ "./src/admin/index.js":
/*!****************************!*\
  !*** ./src/admin/index.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var flarum_extend__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flarum/extend */ "flarum/extend");
/* harmony import */ var flarum_extend__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(flarum_extend__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/app */ "flarum/app");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _addPermissions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addPermissions */ "./src/admin/addPermissions.js");
/* harmony import */ var _common_models_StripeUsers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../common/models/StripeUsers */ "./src/common/models/StripeUsers.js");
/* harmony import */ var _common_models_TagPlan__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../common/models/TagPlan */ "./src/common/models/TagPlan.js");
/* harmony import */ var flarum_components_AdminNav__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! flarum/components/AdminNav */ "flarum/components/AdminNav");
/* harmony import */ var flarum_components_AdminNav__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(flarum_components_AdminNav__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var flarum_components_AdminLinkButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! flarum/components/AdminLinkButton */ "flarum/components/AdminLinkButton");
/* harmony import */ var flarum_components_AdminLinkButton__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(flarum_components_AdminLinkButton__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_PaywallPage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/PaywallPage */ "./src/admin/components/PaywallPage.js");





flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.initializers.add('cmc-paywall', function () {
  Object(_addPermissions__WEBPACK_IMPORTED_MODULE_2__["default"])();
  flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.store.models['stripeusers'] = _common_models_StripeUsers__WEBPACK_IMPORTED_MODULE_3__["default"];
  flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.store.models['tag_plan'] = _common_models_TagPlan__WEBPACK_IMPORTED_MODULE_4__["default"];
});



flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.routes.paywall = {
  path: '/paywall',
  component: _components_PaywallPage__WEBPACK_IMPORTED_MODULE_7__["default"].component()
};

flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.extensionSettings['cmc-paywall'] = function () {
  return m.route(flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.route('paywall'));
};

Object(flarum_extend__WEBPACK_IMPORTED_MODULE_0__["extend"])(flarum_components_AdminNav__WEBPACK_IMPORTED_MODULE_5___default.a.prototype, 'items', function (items) {
  items.add('paywall', flarum_components_AdminLinkButton__WEBPACK_IMPORTED_MODULE_6___default.a.component({
    href: flarum_app__WEBPACK_IMPORTED_MODULE_1___default.a.route('paywall'),
    icon: 'fas fa-bars',
    children: 'Paywall',
    description: 'Add a paywall using Braintree SDK to Flarum tags/groups'
  }));
});

/***/ }),

/***/ "./src/common/models/StripeUsers.js":
/*!******************************************!*\
  !*** ./src/common/models/StripeUsers.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return StripeUsers; });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/app */ "flarum/app");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/Model */ "flarum/Model");
/* harmony import */ var flarum_Model__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_Model__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/utils/mixin */ "flarum/utils/mixin");
/* harmony import */ var flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! flarum/utils/computed */ "flarum/utils/computed");
/* harmony import */ var flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4__);






var StripeUsers =
/*#__PURE__*/
function (_mixin) {
  Object(_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(StripeUsers, _mixin);

  function StripeUsers() {
    return _mixin.apply(this, arguments) || this;
  }

  return StripeUsers;
}(flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3___default()(flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a, {
  stripe_id: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('stripe_id'),
  user_id: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('user_id'),
  card_brand: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('card_brand'),
  card_last_four: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('card_last_four'),
  trial_ends_at: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('trial_ends_at', flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.transformDate)
}));



/***/ }),

/***/ "./src/common/models/TagPlan.js":
/*!**************************************!*\
  !*** ./src/common/models/TagPlan.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TagPlan; });
/* harmony import */ var _babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inheritsLoose */ "./node_modules/@babel/runtime/helpers/esm/inheritsLoose.js");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flarum/app */ "flarum/app");
/* harmony import */ var flarum_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flarum_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flarum_Model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flarum/Model */ "flarum/Model");
/* harmony import */ var flarum_Model__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flarum_Model__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! flarum/utils/mixin */ "flarum/utils/mixin");
/* harmony import */ var flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! flarum/utils/computed */ "flarum/utils/computed");
/* harmony import */ var flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(flarum_utils_computed__WEBPACK_IMPORTED_MODULE_4__);






var TagPlan =
/*#__PURE__*/
function (_mixin) {
  Object(_babel_runtime_helpers_esm_inheritsLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(TagPlan, _mixin);

  function TagPlan() {
    return _mixin.apply(this, arguments) || this;
  }

  return TagPlan;
}(flarum_utils_mixin__WEBPACK_IMPORTED_MODULE_3___default()(flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a, {
  tag_id: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('tag_id'),
  stripe_plan: flarum_Model__WEBPACK_IMPORTED_MODULE_2___default.a.attribute('stripe_plan')
}));



/***/ }),

/***/ "flarum/Component":
/*!**************************************************!*\
  !*** external "flarum.core.compat['Component']" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['Component'];

/***/ }),

/***/ "flarum/Model":
/*!**********************************************!*\
  !*** external "flarum.core.compat['Model']" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['Model'];

/***/ }),

/***/ "flarum/app":
/*!********************************************!*\
  !*** external "flarum.core.compat['app']" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['app'];

/***/ }),

/***/ "flarum/components/AdminLinkButton":
/*!*******************************************************************!*\
  !*** external "flarum.core.compat['components/AdminLinkButton']" ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/AdminLinkButton'];

/***/ }),

/***/ "flarum/components/AdminNav":
/*!************************************************************!*\
  !*** external "flarum.core.compat['components/AdminNav']" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/AdminNav'];

/***/ }),

/***/ "flarum/components/Button":
/*!**********************************************************!*\
  !*** external "flarum.core.compat['components/Button']" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/Button'];

/***/ }),

/***/ "flarum/components/Modal":
/*!*********************************************************!*\
  !*** external "flarum.core.compat['components/Modal']" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/Modal'];

/***/ }),

/***/ "flarum/components/PermissionDropdown":
/*!**********************************************************************!*\
  !*** external "flarum.core.compat['components/PermissionDropdown']" ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/PermissionDropdown'];

/***/ }),

/***/ "flarum/components/PermissionGrid":
/*!******************************************************************!*\
  !*** external "flarum.core.compat['components/PermissionGrid']" ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/PermissionGrid'];

/***/ }),

/***/ "flarum/components/Select":
/*!**********************************************************!*\
  !*** external "flarum.core.compat['components/Select']" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/Select'];

/***/ }),

/***/ "flarum/components/Switch":
/*!**********************************************************!*\
  !*** external "flarum.core.compat['components/Switch']" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['components/Switch'];

/***/ }),

/***/ "flarum/extend":
/*!***********************************************!*\
  !*** external "flarum.core.compat['extend']" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['extend'];

/***/ }),

/***/ "flarum/utils/ItemList":
/*!*******************************************************!*\
  !*** external "flarum.core.compat['utils/ItemList']" ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['utils/ItemList'];

/***/ }),

/***/ "flarum/utils/computed":
/*!*******************************************************!*\
  !*** external "flarum.core.compat['utils/computed']" ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['utils/computed'];

/***/ }),

/***/ "flarum/utils/mixin":
/*!****************************************************!*\
  !*** external "flarum.core.compat['utils/mixin']" ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['utils/mixin'];

/***/ }),

/***/ "flarum/utils/saveSettings":
/*!***********************************************************!*\
  !*** external "flarum.core.compat['utils/saveSettings']" ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['utils/saveSettings'];

/***/ }),

/***/ "flarum/utils/string":
/*!*****************************************************!*\
  !*** external "flarum.core.compat['utils/string']" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = flarum.core.compat['utils/string'];

/***/ })

/******/ });
//# sourceMappingURL=admin.js.map