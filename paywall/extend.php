<?php

namespace CMC\Paywall;

use Flarum\Extend;
use Illuminate\Contracts\Events\Dispatcher;
use Flarum\Foundation\Application;
use CMC\Paywall\StripeUser;


return [

  (new Extend\Frontend('forum'))
        
        ->css(__DIR__ . '/less/forum.less')
        ->js(__DIR__ . '/js/dist/forum.js'),

    (new Extend\Frontend('admin'))
        ->js(__DIR__ . '/js/dist/admin.js')
        ->css(__DIR__ . '/less/admin.less'),
       

    function (Dispatcher $events, Application $app) {
        
        

       /* $la = "test";
        $app->register(StripeServiceProvider::class);
        $app->alias('Stripe', Stripe::class);
      
        Facade::setFacadeApplication($app);*/

        //$events->subscribe(Access\DiscussionPolicy::class);
        $events->subscribe(Access\PostPolicy::class);
        $events->subscribe(Listeners\SavingPremiumDiscussion::class);
        $events->subscribe(Listeners\DiscussionAttributes::class);
        

/*
        $usr = StripeUser::build(1, '', '1234', '');
        if ($usr) {

            $asdf = $usr->createAsStripeCustomer();
            $usr->save();
        }


*/
    }
       
];


        