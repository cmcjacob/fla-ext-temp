<?php

namespace CMC\Paywall\Listeners;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Flarum\Discussion\Discussion;
use Flarum\Discussion\Event\Saving as DiscussionSaving;
use Flarum\Settings\SettingsRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Flarum\Tags\Tag;
use Flarum\User\Exception\PermissionDeniedException;

use CMC\Paywall\TagPlan;

class SavingPremiumDiscussion {

    protected $stripes;

    /**
     * @var SettingsRepositoryInterface
     */
     protected $settings;
    
    public function subscribe(Dispatcher $events)
    {
        $events->listen(DiscussionSaving::class, [$this, 'whenSaving']);
    }

    public function __construct(SettingsRepositoryInterface $settings) {
        $this->settings = $settings;
    }


   
    public function whenSaving(DiscussionSaving $event) {
        $discussion = $event->discussion;
        $actor = $event->actor;



        $stripe_plan = $this->settings->get('cmc.paywall.stripe-plan-id', '');

        if ($stripe_plan !== '') {
            $planObj = TagPlan::where('stripe_plan', $stripe_plan)->first();

            if ($planObj) {

                $tags = collect(Arr::get($event->data, 'relationships.tags.data', []))
                ->map(function ($in) {
                    return (int) $in['id'];
                });

                foreach($tags as $tag) {
                    if ($planObj->tag_id === $tag) {

                       if ($actor->cannot('cmc.paywall.create-premium-post')) {
                            throw new PermissionDeniedException('You are not allowed to post Premium Content. Remove the Premium tag and try again.');
                       }

                            $discussion->is_premium = true;

                       }

                        
                        

                    
                }
            }
        }
    }
}

