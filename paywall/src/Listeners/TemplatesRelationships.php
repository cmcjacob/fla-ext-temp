<?php

namespace CMC\Templates\Listeners;

use Flarum\Api\Controller\ShowForumController;
use Flarum\Api\Serializer\ForumSerializer;
use Flarum\Api\Event\WillGetData;
use Flarum\Event\GetApiRelationship;
use Flarum\Api\Event\WillSerializeData;
use Flarum\Settings\SettingsRepositoryInterface;
use CMC\Templates\Api\Serializer\TemplateSerializer;
use CMC\Templates\Api\Controller\Template;
use Illuminate\Contracts\Events\Dispatcher;

class TemplatesRelationships
{
    /**
     * @var SettingsRepositoryInterface
     */
    protected $settings;
    /**
     * @param SettingsRepositoryInterface $settings
     */
    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }
    /**
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(GetApiRelationship::class, [$this, 'GetApiRelationship']);
        $events->listen(WillSerializeData::class, [$this, 'WillSerializeData']);
        $events->listen(WillGetData::class, [$this, 'includeTemplatesRelationship']);
    }
    /**
     * @param WillSerializeData $event
     */
    public function GetApiRelationship(GetApiRelationship $event)
    {
        if ($event->isRelationship(ForumSerializer::class, 'templates')) {
           
            return $event->serializer->hasMany($event->model, TemplateSerializer::class, 'templates');
        }
    }
    /**
     * @param WillSerializeData $event
     */
    public function WillSerializeData(WillSerializeData $event)
    {
        
        if ($event->isController(ShowForumController::class)) {
            $event->data['templates'] = Template::get();
        }
    }
    /**
     * @param WillGetData $event
     */
    public function includeTemplatesRelationship(WillGetData $event)
    {
        if ($event->isController(ShowForumController::class)) {
            
            $event->addInclude(['templates']);
        }
    }
}