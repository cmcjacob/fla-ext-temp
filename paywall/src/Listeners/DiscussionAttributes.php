<?php
namespace CMC\Paywall\Listeners;

use Flarum\Api\Event\Serializing;
use Flarum\Api\Serializer\DiscussionSerializer;
use Flarum\Api\Serializer\ForumSerializer;
use Flarum\Settings\SettingsRepositoryInterface;
use Illuminate\Contracts\Events\Dispatcher;

class DiscussionAttributes
{
        
    public function subscribe(Dispatcher $events)
    {
        $events->listen(Serializing::class, [$this, 'prepareApiAttributes']);
    }

    
    public function prepareApiAttributes(Serializing $event)
    {
        if ($event->isSerializer(DiscussionSerializer::class)) {
            $canSee = $event->actor->can('cmc.paywall.view-premium-post', $event->model);
            $event->attributes['canSeePremiumContent'] = $canSee;
            $event->attributes['isPremium'] = ($event->model->is_premium) ? true : false;

        } else if ($event->isSerializer(ForumSerializer::class)) {

            // add the stripe key/plan attribute if its a potentially non-subscribed user
            $canSee = $event->actor->can('cmc.paywall.view-premium-post', $event->model);
            if (! $canSee) {
                $settings = app(SettingsRepositoryInterface::class);
                $event->attributes['cmc.paywall.page'] = [$settings->get('cmc.paywall.pay-page-title'), $settings->get('cmc.paywall.pay-page-content')];
                $event->attributes['cmc.paywall.subscribe'] = [$settings->get('cmc.paywall.stripe-plan-id'), $settings->get('cmc.paywall.live-publishable-key')];
            }
        }
    }
  
}