<?php

namespace CMC\Paywall\Access;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Flarum\Event\ScopeModelVisibility;
use Flarum\Discussion\Discussion;
use Flarum\Post\Post;
use CMC\Paywall\StripeUserRepository;

class PostPolicy {

    protected $model = Post::class;
    protected $stripes;
    
    public function subscribe(Dispatcher $events)
    {
        $events->listen(ScopeModelVisibility::class, [$this, 'scopeVisible']);
        $this->stripes = new StripeUserRepository();
    }

   
    public function scopeVisible(ScopeModelVisibility $event) {
        $actor = $event->actor;
        $query = $event->query;

       

        if ($event->query->getModel() instanceof $this->model) {
            $bindings = $query->getBindings();
            if ($bindings) {
                $discussionID = strval($bindings[0]);
                if(is_numeric($discussionID)) {               
                    $discussion = Discussion::findOrFail($discussionID);
                    if ($discussion && $discussion->is_premium) {
                        if (! $this->stripes->assertSubscribed($actor)) {
                            $query->whereRaw('FALSE');
                        }
                    }
                }
               
            } 

       }
    }
}