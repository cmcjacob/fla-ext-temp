<?php

namespace CMC\Paywall;

use Flarum\User\User;
use Illuminate\Database\Eloquent\Builder;

class StripeUserRepository
{
    /**
     * Get a new query builder for the tags table.
     *
     * @return Builder
     */
    public function query()
    {
        return StripeUser::query();
    }

    public function assertSubscribed(User $actor) {

        if ($this->findByActor($actor))
          return true;

        return false;
    }
    
    /**
     * Find a stripeuser by stripe_user ID, optionally making sure it is visible to a certain
     * user, or throw an exception.
     *
     * @param int $id
     * @param User $actor
     * @return Tag
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findByActor(User $actor)
    {
        if (! $actor->exists) 
            return false;

        $q = $this->query()->selectRaw('MAX(user_id) AS user_id')
            ->where('user_id', $actor->id)
            ->first();

        if (! $q->user()) {
            return false;
        }

        return ( ($q->user()->id === $actor->id) ? $q : false ) ;
    }

    public function findByStripeID($id, User $actor = null)
    {
        $query = StripeUser::where('stripe_id', $id)->firstOrFail();
        return $query;
        //return $this->scopeVisibleTo($query, $actor)->firstOrFail();
    }

    /**
     * Find all StripeUsers, optionally making sure they are visible to a
     * certain user.
     *
     * @param User|null $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(User $user = null)
    {
        $query = StripeUser::query();
        return $this->scopeVisibleTo($query, $user)->get();
    }
   
}