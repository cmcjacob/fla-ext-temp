<?php

namespace CMC\Paywall;
use Flarum\Database\AbstractModel;
use Flarum\Foundation\EventGeneratorTrait;
use Flarum\Tags\Tag;
use Illuminate\Database\Eloquent\Builder;
/**
 * @property int $user_id
 * @property int $tag_id
 * @property \Carbon\Carbon|null $marked_as_read_at
 * @property bool $is_hidden
 * @property Tag $tag
 * @property User $user
 */

class TagPlan extends AbstractModel
{
    use EventGeneratorTrait;
    /**
     * {@inheritdoc}
     */
    protected $table = 'tag_plan';
    /**
     * Define the relationship with the tag that this state is for.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
    /**
     * Define the relationship with the user that this state is for.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Set the keys for a save update query.
     *
     * @param Builder $query
     * @return Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $query->where('tag_id', $this->tag_id)
              ->where('user_id', $this->user_id);
        return $query;
    }
}