import {extend} from 'flarum/extend';
import app from 'flarum/app';
import TemplatesPage from './components/TemplatesPage';
import AdminNav from 'flarum/components/AdminNav';
import AdminLinkButton from 'flarum/components/AdminLinkButton';

export default function () {
    app.routes.templates = { path: '/templates', component: TemplatesPage.component() };
    app.extensionSettings['cmc-templates'] = () => m.route(app.route('templates'));

    extend(AdminNav.prototype, 'items', items => {
        items.add('templates', AdminLinkButton.component({
          href: app.route('templates'),
          icon: 'fas fa-bars',
          children: 'Templates',
          description: 'Customize Flarum even more with client-side HTML templates.',
        }));
      });
}