import app from 'flarum/app';
import Component from 'flarum/Component';
import Button from 'flarum/components/Button';
import EditLinkModal from './EditTemplateModal';
var ace = require('brace');
require('brace/mode/html');
require('brace/ext/language_tools');
require('brace/ext/error_marker');
require('brace/snippets/html');
require('brace/theme/dracula'); 

const TEMPLATE_DATA = { 
    '0': { slug: 'asd', shortName: 'Index Page', description: 'Index Page', type: 0 },
    '1': { slug: 'asd2', shortName: 'Discussion List Item', description: 'a5sdf2', type: 1 },
    '2': { slug: 'asd33', shortName: 'Discussion', description: 'a5sdf2', type: 2 },
    '3': { slug: 'asd4', shortName: 'Header', description: 'asd3f2', type: 3 }
}

const availableProps = {
    '1': {
        defaultProps: ['title', 'username', 'discussionURL', 'userID', 'avatarURL'],
        extensionProps: ['views', 'likes', 'excerpt', 'tags'],
        specialProps: ['mason#']
    }
}

var availableTemplates = [true, true, true, true];
var localStore = null;
var CurrentTemplate = -1;
var CurrentSource = null;
var editor;

function renderTemplatePropList(template) {
    let propArray = Array();

    if (availableProps[template]) {

        availableProps[template].defaultProps.map(prop=>{            
            propArray.push(
                <li>
                    <div className="tag selected orange">
                        {`{`}{prop}{`}`}
                    </div>
                </li>
            );
        });

        availableProps[template].extensionProps.map(prop=>{            
            propArray.push(
                <li>
                    <div className="tag selected">
                        {`{`}{prop}{`}`}
                    </div>
                </li>
            );
        });

        availableProps[template].specialProps.map(prop=>{            
            propArray.push(
                <li>
                    <div className="tag selected teal">
                        {`{`}{prop}{`}`}
                    </div>
                </li>
            );
        });

        return (
            <ul className="taglist">
                {propArray}
            </ul>
        );
    }
    return '';
}   
   

function valid(id) { return (id < 4 && id > -1); }
function shortName (id) { 
    return (valid(id)) ? TEMPLATE_DATA[id].shortName : 'undefined' };

function isAny(id, payload) {
    for (let idx=0; idx<=4; idx++) {
        if (payload[idx] && (payload[idx].data) &&
            (payload[idx].data.attributes) &&
                (payload[idx].data.attributes.template_type === id)) {
                    return true;
            }
        }
        return false;
    }


function updateAvailableTemplates(payload) { 
    let blanco = [true, true, true, true];
    localStore = payload;
    for (let idx=0; idx<=4; idx++) {
        if (isAny(idx, payload)) blanco[idx] = false;
    }
    availableTemplates = blanco;
}       

function countAvailableTemplates() {
    let ct=0;
    for (let idx=0; idx<=4; idx++) 
        if ( availableTemplates[idx] == false) ct++;
    return ct;    
}

function handleSaveTemplate(template) {
    if (confirm('Are you sure you wish to overwrite the source for "' + shortName(template) + '"?\n\nNote: This action cannot be reversed.')) {
        console.log('updating source for ' + template + ' to:');
        console.log(CurrentSource);
        let t = currentTemplate();
        t.save({
                    template_type: t.data.attributes.template_type,
                    source: CurrentSource,
                    is_enabled: true,
                })
                .then(
                    response => {
                        
                        alert('Finished');
                    }
                );

    }
}

function handleTemplateClick(template) {
    CurrentTemplate = template;
    if (valid(template)) {
        let src = getSourceFromStore(template);
        console.log(src)
        editor.setValue(src);
    }
}

function currentTemplate() {
    return getTemplateFromStore(CurrentTemplate);
}

function getTemplateFromStore(template) {
    let temp = null;
    localStore.map(t => { 
        if (t.data.attributes.template_type == template) 
            temp = t;
    });
    return temp;
}

function getSourceFromStore(template) {
    let t = getTemplateFromStore(template);
    return (t.data.attributes.source) ? (t.data.attributes.source) : '';
}

function TemplateItem(template) {

    return (
        <tr> 
            <td width="90%" onClick={handleTemplateClick}> {shortName(template.template_type())}</td>
                <td>
                    {Button.component ({
                        className: 'Button Button--template',
                        icon: 'fas fa-pencil-alt',
                        onclick: (e=> { handleTemplateClick(template.template_type()) }
                        ),
                    })
                };
            </td>
        </tr>
    )
}

const renderCodeFlask = () => {
    if (!editor) {

        editor = ace.edit('editor', {
            enableSnippets: true,
            autoScrollEditorIntoView: true,
            vScrollBarAlwaysVisible: true,
            enableBasicAutocompletion: true

        });
        editor.getSession().setMode('ace/mode/html');
        editor.setTheme('ace/theme/dracula');
        editor.session.setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setReadOnly(false);  // false to make it editable

        editor.session.on('change', function(delta) {
            CurrentSource = editor.getValue();
        });
        console.log('HTML editor loaded');
    }
    
}

function renderAddTemplateButton() {
    if (countAvailableTemplates() > 0) { 
        return (
             Button.component({
                className: 'Button Button--templates',
                icon: 'fas fa-plus',
                children: 'Add Template',            
                onclick: () => app.modal.show(new EditLinkModal()),
            })
        );
    } else {
        return(
            Button.component({
                className: 'Button Button--templates',
               onclick: () => app.modal.show(new EditLinkModal()),
                icon: 'fas fa-plus',
                children: 'Add Template',            
            })
        );   
    }
    
}

export default class TemplatesPage extends Component {
    init() {
        // Used in the header
        app.current = this;
    }

    view() {
        return (
            <div className="TemplatesPage">  
            <div className="TemplatesPage-header">
                    <div className="container">
                        <p>Install, uninstall, and modify flarum templates here.</p>
                            {Button.component ({
                                className: 'Button Button--primary',
                                icon: 'fas fa-pencil-alt',
                                children: 'Install New Template',
                                onclick: (e=> { handleSaveTemplate(CurrentTemplate) }
                                ),
                            })
                        };                        
                    </div>
                </div> 

            <div className="container">
  <main className="grid">
    <article>
      <img src="https://i.imgur.com/2K2dTFN.jpg" alt="Sample photo" />
      <i className="fa fa-list-alt"></i>
      <div className="text">
        <h3>GoldCapped</h3>
        <p>This template modifies the Discussion List Item and conditionally shows thread covers provided by users as Mason fields.  Elements are also repositioned.</p>
        {Button.component ({
                                className: 'Button Button--primary',
                                icon: 'fas fa-pencil-alt',
                                children: 'Edit Template',
                                onclick: (e=> { handleSaveTemplate(CurrentTemplate) }
                                )})}
      </div>
    </article>
   
  </main>
</div></div>
            )
    }

    view2() {
        return (
            <div className="TemplatesPage">  
            {updateAvailableTemplates(app.store.all('templates'))}
                <div className="TemplatesPage-header">
                    <div className="container">
                        <p>Install, uninstall, and modify flarum templates here.</p>
                            {Button.component ({
                                className: 'Button Button--primary',
                                icon: 'fas fa-pencil-alt',
                                children: 'Save ' + shortName(CurrentTemplate),
                                onclick: (e=> { handleSaveTemplate(CurrentTemplate) }
                                ),
                            })
                        };                        
                    </div>
                </div> 
                <div className="TemplatesPage-list">
                    <div className="container">
                        {renderTemplatePropList(CurrentTemplate)}
                        <br/>
                        <div className="TemplateItems">
                            <div id="codebox">
                                <div id="editor" className="editor"/>
                                    {setTimeout(renderCodeFlask, 500)} 
                                    <div id="CreateTemplate-col">
                                        <table class="flat-table flat-table-1">
	                                        <thead>
                                                <th className="FullWidth" colspan="2">
                                                   {renderAddTemplateButton()}
                                                </th>
	                                        </thead>
	                                    <tbody>
                                            {localStore.map(TemplateItem)}  
	                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    
}