import app from 'flarum/app';
import Component from 'flarum/Component';
import Button from 'flarum/components/Button';
import LoadTemplateModal from './LoadTemplateModal';
import Switch from 'flarum/components/Switch';
import EditTemplateSourceModal from './EditTemplateSourceModal';

const TEMPLATE_DATA = { 
    '0': { slug: 'asd', shortName: 'Index Page', description: 'Index Page', type: 0 },
    '1': { slug: 'asd2', shortName: 'Discussion List Item', description: 'a5sdf2', type: 1 },
    '2': { slug: 'asd33', shortName: 'Discussion', description: 'a5sdf2', type: 2 },
    '3': { slug: 'asd4', shortName: 'Header', description: 'asd3f2', type: 3 }
}


var availableTemplates = [true, true, true, true];
var localStore = null;

function valid(id) { return (id < 4 && id > -1); }
function shortName (id) { 
    return (valid(id)) ? TEMPLATE_DATA[id].shortName : 'undefined' };

function isAny(id, payload) {
    for (let idx=0; idx<=4; idx++) {
        if (payload[idx] && (payload[idx].data) &&
            (payload[idx].data.attributes) &&
                (payload[idx].data.attributes.template_type === id)) {
                    return true;
            }
        }
        return false;
    }


function updateAvailableTemplates(payload) { 
    let blanco = [true, true, true, true];
    localStore = payload;
    for (let idx=0; idx<=4; idx++) {
        if (isAny(idx, payload)) blanco[idx] = false;
    }
    availableTemplates = blanco;
}       


export default class TemplatesPage extends Component {

    init() {
        // Used in the header
        app.current = this;
        this.changes = false;
        this.isEnabled = {};
    }

    afterLoad() {
        console.log('callback');
        m.redraw(true);
    }

    handleEnabledChange(enabled, template) {
        template.save (
            {
                is_enabled: enabled
            }
        )
    }

    handleEditTemplate(template) {
        var modal = new EditTemplateSourceModal({template: template});
        //modal.fillIn(template);
        app.modal.show(modal);
    }

    renderTemplate(template) {
        console.log(window.location);
        
       

        return (

            <article key={template.data.id}>
                <img src={template.data.attributes.previewPath} alt="Preview" />
                <i className="fa fa-list-alt"></i>
                <div className="text">
                    <h3>{template.data.attributes.name}</h3>
                    <p>{template.data.attributes.description}</p>
                    
                    <div className="Template-Toggle"> { Switch.component({
                state: template.data.attributes.is_enabled,
              
                onchange: e => this.handleEnabledChange(e, template)
              })}</div>


               
                    {Button.component ({
                                            className: 'Button Button--primary hasIcon',
                                            icon: 'fas fa-pencil-alt',
                                            children: 'Edit Template',
                                            onclick: (e=> { this.handleEditTemplate(template) }
                                            )})}
                </div>
              
            </article>
        );
    }


    renderAllTemplates() {
        updateAvailableTemplates(app.store.all('templates'));
        return(localStore.map (thisTemplate => {
            return(this.renderTemplate(thisTemplate));
        }));
    }

    renderLoadTemplateButton() {
        const btn = Button.component ({
            className: 'Button Button--primary',
            icon: 'fas fa-pencil-alt',
            children: 'Load Template Package',
            onclick: () => app.modal.show(new LoadTemplateModal(window.location))
        });
        return(btn)
    }

    view() {
        return (
            <div className="TemplatesPage">  
            <div className="TemplatesPage-header">
                    <div className="container">
                        <p>Install, uninstall, and modify flarum templates here.</p>
                        {this.renderLoadTemplateButton()}   
                    </div>
                </div> 

            <div className="container">
                <main className="grid">

                   {this.renderAllTemplates()}
                
                </main>
                </div></div>
            )
    }
    
}