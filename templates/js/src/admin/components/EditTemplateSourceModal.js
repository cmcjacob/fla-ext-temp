import app from 'flarum/app';
import Modal from 'flarum/components/Modal';
import Button from 'flarum/components/Button';

var ace = require('brace');
require('brace/mode/html');
require('brace/ext/language_tools');
require('brace/ext/error_marker');
require('brace/snippets/html');
require('brace/theme/dracula'); 

/*
function handleSaveTemplate(template) {
    if (confirm('Are you sure you wish to overwrite the source for "' + shortName(template) + '"?\n\nNote: This action cannot be reversed.')) {
        console.log('updating source for ' + template + ' to:');
        console.log(CurrentSource);
        let t = currentTemplate();
        t.save({
                    template_type: t.data.attributes.template_type,
                    source: CurrentSource,
                    is_enabled: true,
                })
                .then(
                    response => {
                        
                        alert('Finished');
                    }
                );

    }
}*/

/* We will revive this if the editor gives us problems again..
function refreshEditor() {
    if (editor) {
        setTimeout(function() {
            editor.refresh();
        }, 500 );

        editor.setSize('100%', 300);    
    }
}*/

var currentSource = null, editor;


export default class EditTemplateSourceModal extends Modal {

    init() {
        editor = null;
        //this.source = null;
        currentSource = null;
    }

    template() {
        return ( (this.props && this.props.template) ? (this.props.template) : (false) ); 
    }

    source() {
        return ( (editor) ? (editor.getValue()) : (null) );
    }

    className() {
        return 'EditTemplateModal Modal--large';
    }

    title() {
        return 'Edit Template Source';
    }

    onready() {
        this.createCodeFlask();
    }

    content() {
        return (
            <div className="Modal-body">
                <div className="Form">
                    <div className="Form-group">
                        <label>Template Source</label>
                        <div id="codebox">
                            <div id="editor" className="editor"/>      
                        </div>
                    </div>
                    <div className="Form-group">
                        {Button.component({
                            type: 'submit',
                            className: 'Button Button--primary EditTemplateModal-save',
                            loading: this.loading,
                            children: 'Save Changes'
                        })}
                    { /*refreshEditor()  // uncomment if editor gives issues */ }
                </div> 
            </div>
            </div>
        );
    }
                
    onsubmit(e) {
        e.preventDefault();
        if (this.template()) {
            this.loading = true;
            this.template()
                .save({
                    source: this.source()
                })
                .then(
                    () => this.hide(),
                    response => {
                        this.loading = false;
                        this.handleErrors(response);
                    }
                );
            }
        }
    

    delete() {
        if (confirm('Are you sure you wish to delete this template?')) {
            this.template.delete().then(() => m.redraw());
            this.hide();
        }
    }

    isDismissible() {
        return true;
    }

    onhide() {
       
        // TODO: Check if template source has changed, and prompt to save
        if (this.source() !== this.template().data.attributes.source) {
            alert('Dismissing changes');
        }
        editor = null;
    }

    createCodeFlask() {
        if (!this.template()) return;
        
        if (!editor) {
           
            editor = ace.edit('editor', {
                enableSnippets: true,
                autoScrollEditorIntoView: true,
                vScrollBarAlwaysVisible: true,
                enableBasicAutocompletion: true
            });
            editor.getSession().setMode('ace/mode/html');
            editor.setTheme('ace/theme/dracula');
            editor.session.setUseWrapMode(true);
            editor.setShowPrintMargin(false);
            editor.setReadOnly(false);  
            editor.setValue(this.template().data.attributes.source);
        }      
    }
}