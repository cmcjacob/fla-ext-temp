import app from 'flarum/app';
import Modal from 'flarum/components/Modal';
import Button from 'flarum/components/Button';

const TYPE_AVAILABLE = 10;
const TYPE_UNAVAILABLE = 20;

const TEMPLATE_DATA = { 
    '0': { slug: 'asd', shortName: 'Index Page', description: 'asdf42', type: 0 },
    '1': { slug: 'asd2', shortName: 'Discussion List Item', description: 'a5sdf2', type: 1 },
    '2': { slug: 'asd33', shortName: 'Discussion', description: 'a5sdf2', type: 2 },
    '3': { slug: 'asd4', shortName: 'Header', description: 'asd3f2', type: 3 }
}


function valid(id) { return ( id < 4 && id > -1); }
function slug (id) { return (valid(id)) ? TEMPLATE_DATA[id].slug : undefined };
function description  (id) { return (valid(id)) ? TEMPLATE_DATA[id].description : undefined };
function shortName (id) { return (valid(id)) ? TEMPLATE_DATA[id].shortName : undefined };
function getOne  (id) { return (valid(id) ? TEMPLATE_DATA : undefined) };
function getMany() { return TEMPLATE_DATA };
function poolSize ()  { return TEMPLATE_DATA.length };

function isAny(id, payload) {
    for (let idx=0; idx<=4; idx++) {
        if (payload[idx] && (payload[idx].data) &&
            (payload[idx].data.attributes) &&
                (payload[idx].data.attributes.template_type === id)) {
                    return true;
            }
        }
        return false;
    }

var localCopy = Array();

function freeAfterPayload(payload) { 
    let blanco = [true, true, true, true];
    for (let idx=0; idx<=4; idx++) {
        if (isAny(idx, payload)) blanco[idx] = false;
    }
    localCopy = blanco;
    return blanco; 
}  

var editor=null;

function handleTypeChange(data) {
    console.log(data);
}

function renderTypeDropdownItem(templateType) {
    console.log('should be checking against ' + templateType)
    console.log(localCopy);
    if (localCopy[templateType] === true) {
            console.log(TEMPLATE_DATA[templateType].type);

        return (
            <option value={TEMPLATE_DATA[templateType].type}
            onBlur={handleTypeChange}
                id={TEMPLATE_DATA[templateType].type}>
                {TEMPLATE_DATA[templateType].shortName}
            </option>
        );
    }
}


function refreshEditor() {
    if (editor) {
        setTimeout(function() {
            editor.refresh();
        }, 500 );

        editor.setSize('100%', 300);    
    }
}

function templateTypeFromName(name) {
    switch (name) {
        case 'Index Page':
            return 0;
            break;
        case 'Discussion List Item':
            return 1;
            break;
        case 'Discussion':
            return 2;
            break;
        case 'Header':
            return 3;
            break;
        default:
            return -1;
            break;
    }
}

export default class EditTemplatesModel extends Modal {
    init() {
        super.init();
        
        this.template = this.props.template || app.store.createRecord('templates');
        this.template_type = m.prop(this.template.template_type() || -1);
        this.source = m.prop(this.template.source() || '<a href="google.com">google</a>')
        this.is_enabled = m.prop(this.template.is_enabled()) || false;
        this.selectedType = -1;
        this.selectedEnabled = this.is_enabled() || false;
        this.newestProps = null;
        console.log(this.template);
    }


    handleTypeChange(type) {
        var selectedText = type.srcElement.options[type.srcElement.selectedIndex].innerHTML;
        var selType = templateTypeFromName(selectedText);
        if (selType > -1) {
            this.selectedType = selType;
        }
    } 

    types(all=true) {
        if (all) {
            return TEMPLATE_DATA;
        } else {
            return TEMPLATE_DATA[all];
        }
    }

    className() {
        return 'EditTemplateModal Modal--small';
    }

    gettype() {
        console.log(this.template_type);
        console.log(this.source);
        const type = this.template_type();
        return type || 0;
    }

    retrievePropUpdates() {
        this.newestProps = app.store.all('templates');
        return(this.newestProps);
    }

    renderAvailableTypes() {
        var availableTemplates = Array(4);
        availableTemplates = freeAfterPayload(this.retrievePropUpdates  ());
        return(
        <select className="TemplatesType-Select" size={4} name="template_type" id="SelectType" onchange={e => this.handleTypeChange(e)} onChange={ e => this.handleTypeChange(e)} >
            { Object.keys(availableTemplates).map(renderTypeDropdownItem) }              
         </select>
        );
    }
        
    

    content() {
        return (
            <div className="Modal-body">
                <div className="Form">
                    <div className="Form-group">
                        <label>Select a template to open.</label>
                       
                        <div class="subject-info-box-1">
                                   { this.renderAvailableTypes() }
                                </div>



                           
                        
               
                        <input
                            className="FormControl"
                            placeholder='placeholder'
                            value = { 
                                (valid(this.selectedType) ? 
                                 TEMPLATE_DATA[this.selectedType].description :
                                 '') }
                          
                        />
                    </div>


                    <div className="Form-group">
                        <div>
                            <label className="checkbox">
                                <input
                                    type="checkbox"
                                    name="is_enabled"
                                    value="1"
                                    checked={this.is_enabled()}
                                    onchange={e => {
                                        if (e.target.checked) {
                                            this.selectedEnabled = true;
                                        } else {
                                            this.selectedEnabled = false;
                                        }
                                    }}
                                />
                                Template is Enabled 
                            </label>
                        </div>
                    </div>

                    <div className="Form-group">
                        {Button.component({
                            type: 'submit',
                            className: 'Button Button--primary EditTemplateModal-save',
                            loading: this.loading,
                            children: 'Edit Template',
                        })}
                        {this.template.exists ? (
                            <button type="button" className="Button EditTemplateModal-delete" onclick={() => this.delete()}>
                                Delete Template
                            </button>
                        ) : (
                            ''
                        )}
                    </div>
                    { refreshEditor() }
                </div> 
              
            </div>
        );
    }
                
    onsubmit(e) {
        e.preventDefault();
        if (!valid(this.selectedType)) {
            this.hide();
            alert('Template type is invalid; aborting');
        } else {
            this.loading = true;

            this.template
                .save({
                    template_type: this.selectedType,
                    source: 'Blank Template',
                    is_enabled: this.selectedEnabled,
                })
                .then(
                    () => this.hide(),
                    response => {
                        this.loading = false;
                        this.handleErrors(response);
                    }
                );
        }
    }

    delete() {
        if (confirm('Are you sure you wish to delete this template?')) {
            this.template.delete().then(() => m.redraw());
            this.hide();
        }
    }
}