import app from 'flarum/app';
import Modal from 'flarum/components/Modal';
import Button from 'flarum/components/Button';






const TEMPLATE_DATA = { 
    '0': { slug: 'asd', shortName: 'Index Page', description: 'asdf42', type: 0 },
    '1': { slug: 'asd2', shortName: 'Discussion List Item', description: 'a5sdf2', type: 1 },
    '2': { slug: 'asd33', shortName: 'Discussion', description: 'a5sdf2', type: 2 },
    '3': { slug: 'asd4', shortName: 'Header', description: 'asd3f2', type: 3 }
}


function valid(id) { return ( id < 4 && id > -1); }


function isAny(id, payload) {
    for (let idx=0; idx<=4; idx++) {
        if (payload[idx] && (payload[idx].data) &&
            (payload[idx].data.attributes) &&
                (payload[idx].data.attributes.template_type === id)) {
                    return true;
            }
        }
        return false;
    }

var localCopy = Array();

function freeAfterPayload(payload) { 
    let blanco = [true, true, true, true];
    for (let idx=0; idx<=4; idx++) {
        if (isAny(idx, payload)) blanco[idx] = false;
    }
    localCopy = blanco;
    return blanco; 
}  

var editor=null;

function handleTypeChange(data) {
    console.log(data);
}


function templateTypeFromName(name) {
    switch (name) {
        case 'Index Page':
            return 0;
            break;
        case 'Discussion List Item':
            return 1;
            break;
        case 'Discussion':
            return 2;
            break;
        case 'Header':
            return 3;
            break;
        default:
            return -1;
            break;
    }
}

let wloc = '';

export default class LoadTemplateModal extends Modal {
    init(w) {
        super.init();
        this.errors = '';
        this.file = '';
        this.wloc = w;
       
    }

    types(all=true) {
        if (all) {
            return TEMPLATE_DATA;
        } else {
            return TEMPLATE_DATA[all];
        }
    }

    className() {
        return 'EditTemplateModal Modal--small';
    }

    gettype() {
        console.log(this.template_type);
        console.log(this.source);
        const type = this.template_type();
        return type || 0;
    }

    handleFileChange(e) {
        let file = e.srcElement.files;
        

        switch (file[0].type) {
            case "application/x-tar":
            case "application/x-gzip":
            case "application/x-zip-compressed":
                console.log('detected proper file-type');
                this.errors = '';
                this.file = file[0];
              
                
                break;
            default:
                this.errors = "File is not a application/x-gzip or x-tar or x-zip type:<br/>" + file[0].type;
                console.log(file[0].type);
                this.file = null;
        }
    
    }

    showErrors() {
        return ( this.errors ? (<span style="color: #ff0000">Error: {this.errors}</span>) : '');
    }
    

    content() {
        return (
            <div className="Modal-body">
                <div className="Form">
                <form enctype="multipart/form-data" method="POST">
                    <div className="Form-group">
                        <label>Load template package</label>
                       
                        <div class="subject-info-box-1">
                                   File should follow <a href="http://google.com">template format guidelines.</a> 
                                </div>



                           
                       
               
                        <input
                            type="file"
                            className="FormControl"
                            name="uploaded_file"
                            onchange={e => this.handleFileChange(e)}
                          
                        />
                    </div>

                    <div className="Form-error">{this.showErrors()}</div>


                    <div className="Form-group">
                        <div>
                            <label className="checkbox">
                                <input
                                    type="checkbox"
                                    name="is_enabled"
                                    value="1"
                                    checked={true}
                                    onchange={e => {
                                        if (e.target.checked) {
                                            this.selectedEnabled = true;
                                        } else {
                                            this.selectedEnabled = false;
                                        }
                                    }}
                                />
                                Enabled
                            </label>
                        </div>
                    </div>

                    <div className="Form-group">
                        {Button.component({
                            type: 'submit',
                            className: 'Button Button--primary EditTemplateModal-save',
                            loading: this.loading,
                            children: 'Load Package',
                            onclick: e => {this.onsubmit(e) }
                        })}
                        
                    </div>
                  </form>
                </div> 
              
            </div>
        );
    }

    failure(message) {
        console.log('failure');
        console.log(message);
        //alert('Failure: ' + message);
        this.errors = message.responseText;
        this.loading = false;
       
        m.redraw(true);
        this.hide();
        console.log(this);
        this.afterLoad();
    }

    success(response) {
        this.loading = false;
        m.redraw(true);
        this.hide();
        m.redraw(true);
        console.log(this);
        this.afterLoad();
    }
                
    onsubmit(e) {
        e.preventDefault();

        let data = new FormData();
        //const data = new FormData();
        data.append('template', this.file);
        console.log(this.file);
        console.log(data.entries('template'));
       
            this.loading = true;

            return app.request({
                method: 'POST',
                url: app.forum.attribute('apiUrl') + '/templates/upload',
                // prevent JSON.stringify'ing the form data in the XHR call
                serialize: raw => raw,
                data
            }).then( () => {
                this.hide(),
                response => {
                    console.log(window.location);
                    window.location.reload(true);

                    this.loading=false;
                    this.handleErrors(response);

                    app.store.find('templates', '').then(template=> {      
                    
                    m.redraw(true);
                    window.location.reload(true);
                   });
                   m.redraw();

                }    
                 
                

            }
            );
        
    }

    
}