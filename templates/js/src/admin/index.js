import {extend} from 'flarum/extend';
import app from 'flarum/app';
import addTemplatesPage from './addTemplatesPage';
import Template from '../common/models/Template';


app.initializers.add('cmcjacob-fardoragh-thread-covers', () => {
    app.store.models.templates = Template;
    addTemplatesPage();
});