import Model from 'flarum/Model';
import mixin from 'flarum/utils/mixin';

export default class Template extends mixin(Model, {
    template_type: Model.attribute('template_type'),
    is_enabled: Model.attribute('is_enabled'),
    source: Model.attribute('source'),
    description: Model.attribute('description'),
    version: Model.attribute('version'), 
    requirements: Model.attribute('requirements'),
    author: Model.attribute('author'),
    name: Model.attribute('name'),
    previewPath: Model.attribute('previewPath')
}) {}
