

// I wrote this because I am only 29% sure of what exactly I'm doing
// (20% of it being client so thats where the logic is.)
// TODO: Improve this insecure, inefficient client-side model of shit

var _TYPE = (s,n,d,t) => { return { slug: s, shortName: n, description: d, type: t } }
const TYPE_AVAILABLE = 10;
const TYPE_UNAVAILABLE = 20;


export const TEMPLATE_DATA = { 
    0: _TYPE (
         'indexPage', 'Index Page',
         'Displays from the root view of your forum.  Contains header, hero, discussion list and side panel.', 0
    ),
    1: _TYPE (
         'discussionList', 'Discussion List',
         'Displays a list of DiscussionListItems on the Index Page.  To change individual entries in the list, use DiscussionListItem instead.', 1
    ),
    2: _TYPE (
         'discussionListItem', 'Discussion List Item',
         'Displays an individual discussion on the Index Page. Changing this does not affect the template of opened discussions.', 2
    ),
    3: _TYPE (
         'discussion', 'Opened Discussion',
         'Displayed when a discussion on the Index Page is clicked.  Cannot be changed yet.', 3 
     ),
     4: _TYPE (
          'welcomeHero', 'Index Welcome Hero',
          'Displayed when a discussion on the Index Page is clicked.  Cannot be changed yet.', 3 
      ) }


export function Types() {

     function valid() { return (id && id < 5 && id > -1); }
     function slug (id) { return (valid(id)) ? TEMPLATE_DATA[id].slug : undefined };
     function description  (id) { return (valid(id)) ? TEMPLATE_DATA[id].description : undefined };
     function shortName (id) { return (valid(id)) ? TEMPLATE_DATA[id].shortName : undefined };
     function getOne  (id) { return (valid(id) ? TEMPLATE_DATA : undefined) };
     function getMany() { return TEMPLATE_DATA };
     function poolSize ()  { return TEMPLATE_DATA.length };

     function availableAfterPayload(payload) { 
          let blanco = Array().fill(true, this.poolSize);
          for (let idx=0; idx<=poolSize.length; idx++) {
               if (!payload[idx]) blanco[idx] = TYPE_UNAVAILABLE;
               blanco[payload[idx].data.attributes.template_type] = TYPE_AVAILABLE
          }
     return blanco; 
     }          

     
};

export default Types

