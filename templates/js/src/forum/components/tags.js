
import app from 'flarum/app';
import tagsLabel from '../helpers/tags';
import Component from 'flarum/Component';

export class Tags extends Component {
    init() {}
    initProps() {}

    view() {
     
      let attrs = Object.assign({}, this.props);

      const tags = this.props.discussion.tags();
      const cls = this.props.classname || 'TemplateTags';

      return(
        <li className='item-template-tags'>
          {tagsLabel(tags)}
         </li>
        )
      }
    }

export default Tags
