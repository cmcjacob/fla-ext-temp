import app from 'flarum/app';
import Component from 'flarum/Component';

export class ToolTip extends Component {

    init() {}
    initProps() {}
  
    view(classname, title, direction, children) {
      return(
        <div className={classname}
          title={title}
          config={function(element) {
            $(element).tooltip({placement:{direction}})}}>
          {children}
        </div>
        )
    }
  }

  export default ToolTip;
