import Dropdown from 'flarum/components/Dropdown';

export default class EditTemplateDropdown extends Dropdown {
    static initProps(props) {
      super.initProps(props);
  
      props.className = 'Edit-Template';
      props.buttonClassName = 'Button Button--user Button--primary';
      props.menuClassName = 'Dropdown-menu--right';
    }
  
    view() {
      this.props.children = this.items().toArray();
  
      return super.view();
    }
  
    getButtonContent() {
      const user = app.session.user;
  
      return [
        avatar(user), ' ',
        <span className="Button-label">{username(user)}</span>
      ];
    }
  
    /**
     * Build an item list for the contents of the dropdown menu.
     *
     * @return {ItemList}
     */
    items() {
      const items = new ItemList();
  
      items.add('logOut',
        Button.component({
          icon: 'fas fa-sign-out-alt',
          children: app.translator.trans('core.admin.header.log_out_button'),
          onclick: app.session.logout.bind(app.session)
        }),
        -100
      );
  
      return items;
    }
  }