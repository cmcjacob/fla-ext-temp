
import Component from 'flarum/Component';

// allows users to use <route> tags to properly route areas
// such as user links and posts
var RouteComponent =  {
    

  
    view: function(vnode) {
      
      return (
        <a href= {route}
        config={function(element) {
                  m.route.apply(this, arguments);
                }}>
         
          {vnode.children}
        </a>
      )
    }
  }

  export default RouteComponent; 
