import app from 'flarum/app';
import Component from 'flarum/Component';
import extractText from 'flarum/utils/extractText';
import humanTime from 'flarum/helpers/humanTime';
import avatar from 'flarum/helpers/avatar';

export class Avatar extends Component {
    init() {
    }
    initProps() {}
 
    view() {
      let attrs = Object.assign({}, this.props);

      if (!this.props.discussion)  { 
        return (
          <div>test
                  </div>)
      }

      let user = this.props.discussion.user();
      let tip_place = this.props.tooltip || 'left';
      let className = this.props.className || 'Template';

    
       return (

         <a href={user ? app.route.user(user) : '#'}
             className="discussion-lastposter"
             title={extractText(app.translator.trans('core.forum.discussion_list.started_text', {user: user, ago: humanTime(this.props.discussion.createdAt())}))}
             config={function(element) {
               $(element).tooltip({placement: tip_place});
               m.route.apply(this, arguments);
             }}>
             
             {avatar(user, {className: className})}
           </a>
           
       );
    }
  }

  export default Avatar;
