import app from 'flarum/app';
import Component from 'flarum/Component';
import listItems from 'flarum/helpers/listItems';

export class Badges extends Component {
    init() {}
    initProps() {}    

    view() {
        let attrs = Object.assign({}, this.props);
        let discussion = this.props.discussion;
        let lst =  listItems(discussion.badges().toArray());
        
        return (
            <ul className="DiscussionListItem2-badges badges">
             {lst}
            </ul>);
           
        
    }
}

export default Badges;
 
