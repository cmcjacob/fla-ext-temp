
import {templateBuilder} from "mithril-template-builder";

var mithrilTemplate = require("mithril-template");


const REGEX_DIGITS          = /\d+/;

export class Template {

      constructor(source, template_type, is_enabled=false, payload=null) {
        this.source_decompiled = source;
        this.template_type = template_type;
        this.is_enabled = is_enabled
        this.payload_newest = payload;
        this.source_compiled = '';
        this.instance = null;
        this.props = null;

          // Payload field type constants
        this.FIELD_TYPE_STATIC       = 1;
        this.FIELD_TYPE_COMPONENT    = 2;
        this.FIELD_TYPE_MASON        = 5;
        this.FIELD_TYPE_UNKNOWN      = 9;
        this.FIELD_TYPE_AGE          = 11;

   



      }

 

      validatePayloadField(field) {
        let ft = this.getPayloadFieldType(field);
        return( { valid: ((ft === this.FIELD_TYPE_UNKNOWN) ? false : true), result: ft })
      }

      getPayloadFieldType (field) {
      
        switch (field) {
          case 'title':
          case 'username':
          case 'userID':
          case 'userURL':
          case 'avatarURL':
          case 'discussionURL':
          case 'views':
          case 'votes':
          case 'excerpt': 
          case 'replycount':
            return this.FIELD_TYPE_STATIC;
            
          case 'controls':
          case 'tags':
          case 'badges':
          case 'userAvatar':
            return this.FIELD_TYPE_COMPONENT;

            
          default:
            if (field.indexOf("mason") > -1)
              return this.FIELD_TYPE_MASON;

            if (field === 'age') 
             return this.FIELD_TYPE_AGE;

            return this.FIELD_TYPE_UNKNOWN;

        }
      }

      getType() {
        return this.template_type;
      }



  // TODO: Allow template-sources to skip this processing if their template
  // doesnt contain any {properties} to {resolve}
  resolveStaticProperties(src) {
    let src_resolved = src;
    var result;
    var rxp = /{([^}]+)}/g;
    while( result = rxp.exec( src_resolved ) ) {
      if ((result) && (result[0])) {
        let fieldType = this.validatePayloadField(result[1]);
        if (fieldType.valid) {
          switch (fieldType.result) {

            case this.FIELD_TYPE_STATIC:
              src_resolved = src_resolved.replace(result[0], this.findOrFailPayload(result[1]));
              break;

            case this.FIELD_TYPE_MASON:

              let masonID = result[1].match(REGEX_DIGITS);
              if (masonID) {
                src_resolved = src_resolved.replace(result[0], this.getMasonFieldAnswer(masonID)); 
                } break;

            default:
              //console.log('still need to handle ' + result[0]);
          }
        }
      }
    }
    return src_resolved;
  }


  // TEMPORARILY DEPRECATED
  // recursively injects one component into another inside a mithril VDOM tree
  // this lets us mix actual flarum components into our own compilation
  // todo: make this function less dangerous with iteration count validation
  inject(obj, obj2, search) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (typeof obj[property] == "object") {
                this.inject(obj[property], obj2, search);
            } else {
              let toStr = String(obj[property]);
                if (toStr.includes(search))  
                  obj[property] = obj2;
            }
        }
    }
  }

  push(payload) {
    if (this.setGlobals) 
      this.setGlobals(payload)
    
    this.instance = payload.props.instance;
    this.payload_newest = payload;
    this.props = payload.props

    if (this.source_decompiled) 
      this.source_compiled = this.resolveStaticProperties(this.source_decompiled);
  }


  // TODO(?): Run tests on mithrilTemplate vs templateBuilder
  // in the event a user doesnt want to use the templateBuilder compiler
  // TEMPORARILY DEPRECATED
  compileOld(chunk, builder=true) {
    return ( (builder) ? (eval(templateBuilder({source: chunk}))) : (m('div', m.trust(chunk))) );
  }

  // Updated
  compile() {
  

    
     return(m('div', eval(mithrilTemplate('<div class="' + this.className + '">' + this.source_compiled + '</div>')) ));
  }
  
  
  findOrFailPayload(field) {
    let result = false;
    try   { result  = this.payload_newest.payload[field]; }
    catch { return false; }
    return result;
  }

  // todo: cache these and move all mason code into a separate component 
  getMasonFieldAnswer(id) {
    let answerField = null, answers;
    answers = this.payload_newest['payload'].answers;
    if (answers) {
      answers.map(answer => {
        if(id == answer.data.relationships.field.data.id)  
          answerField = answer.data.attributes.content;
      });
    }
    return answerField;   
  }


  // nestCheck is deprecated
  injectComponent(component, src, regx, nestCheck='route') {
    this.injectables.push( { search: regx[0], component: component  });
    return src;
  }

}

export default Template;