// TODO: implement controlsComponent

import Template from '../Template';
import Controls from '../../components/controls';
import Avatar from '../../components/avatar';
import Badges from '../../components/badges';
import Tags from '../../components/tags'; 
import AgeComponent from '../../helpers/age';
import Component from 'flarum/Component';
var mithrilTemplate = require("mithril-template");

let payload2 = null;

let avatarComponent = Avatar;
let tagsComponent = Tags;
let badgesComponent = Badges;
let ageComponent = AgeComponent;
 
//TODO: Don't use globals
class routeComponent extends Component {

  view(vnode) { 
    let attrs = Object.assign({}, this.props);
    const cls = this.props.class || '';
    console.log(this.props);

    let to = null;



    if (attrs.to === payload2.props.props.discussion)  {
      
      to = app.route.discussion(
        payload2.props.props.discussion
      )
      }

    return (
      <a class={cls} href= {to}
      config={function(element) {
                m.route.apply(this, arguments);
              }}>
       
        {this.props.children}
      </a>
    )
  }
}



export class DiscussionItem extends Template {

    constructor(src, is_enabled=true) {
        super(src, 1, is_enabled);
        this.xmlSource = '';
        this.injectables = Array();
        this.avatar = null;
        this.badges = null;
        this.controls = null;
        this.route = null;
        this.tags = null;
        this.mithril_view = null;

        // TODO: configure default classname in preferences (?)
        this.className = "Template-DiscussionItem";
    }

    // we must call compile() from this class so the template has access to the discussion components
    compile() {
      let discussion = this.props.props.discussion;
      let instance = this.payload_newest.props.instance;   
 
      this.source_compiled = this.source_compiled.replace('src="null"', 'src="https://via.placeholder.com/80"');
      
      var mason = (id => { if (this.getMasonFieldAnswer(id)) { return true; } else { return false } });
      return(m('div', eval(mithrilTemplate('<div class="' + this.className + '">' + this.source_compiled + '</div>')) ));
    }

    // TODO: move this to Template class
    setGlobals(p=null) {
      if (p) payload2 = p;
    }

    view(redraw=false) {
      return(this.compile());
    } 
  }


export default DiscussionItem;
