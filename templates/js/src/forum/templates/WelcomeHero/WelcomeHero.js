import Template from '../Template';

export default class WelcomeHero extends Template {
    constructor(src, is_enabled=true) {
        super(src, 4, is_enabled);
        this.className = "Template-WelcomeHero"
    }

    // required
    setGlobals(p=null) {
    }
  
    // default compiler
    view(redraw=false) {
       return(this.compile());
    }
}