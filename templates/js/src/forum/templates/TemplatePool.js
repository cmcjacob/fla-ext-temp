import DiscussionItem from './DiscussionListItem/DiscussionItem';
import WelcomeHero  from './WelcomeHero/WelcomeHero';

const ID_SINGLE_COMPONENT = -44;

function valid(a) { return true; }

export class TemplatePool { 
    templateInstancePool = Array();
    sources = {};
  
    constructor()  {
      let templates = app.store.all('templates')
      if (templates) this.init(templates);
      return this;
    }
  
    init(templates) {
      this.templateInstancePool = Array(); // flush
      if (templates) templates.map(template => {
        if ( (template.data) && (template.data.attributes)) {
          let thisType = template.data.attributes.template_type;
          if (valid(thisType)) {
            if (template.data.attributes.is_enabled === true)
              this.add(
                template.data.attributes.source,
                template.data.attributes.template_type,
                false
              );
            }
          }
        });   
    }
  
    templateLimit = () => { return 4 }

    isFull()  {
      return this.templateInstancePool.length >= 99999; 
    }
  
  
    // just gets one
    findOrFailType(type) {
      for (var ii=0; ii<=this.templateInstancePool.length; ii++) {
        if (this.templateInstancePool[ii]) {
          if (this.templateInstancePool[ii].context) {
            if (this.templateInstancePool[ii].context.template_type === type) 
              return this.templateInstancePool[ii].context;
           }
        }
      }      
      return undefined;
    }

    // ID is only used for templated components with multiple instances of same type
    findOrFailID(id) {
        for (var ii=0; ii<=this.templateInstancePool.length; ii++) {
          if (this.templateInstancePool[ii]) {
            if (this.templateInstancePool[ii].ID === id) 
              return this.templateInstancePool[ii].context;
            
          }
        }
        
        return undefined;
    }
  

    isIdInPool(ID) {
      return (this.findOrFailID(ID)) ? true : false;
    }

    isTypeInPool(template_type) {
      return (this.findOrFailType(template_type)) ? true : false;
    }

    countInstance(template_type) {
      let ct = 0;
      let lastInst = null;
      this.templateInstancePool.map(template => {
        if (template.context.template_type === template_type) {
          lastInst = template;
          ct++; 
        }
      })
      return {count: ct, lastInstance: lastInst};
    }
  
    // distributes prop payloads to the right template instance
    // (creates instance if needed)
    push(template_type, payload, ID)  { 

      let isSingle = (ID !== ID_SINGLE_COMPONENT);
      let inst = ( (isSingle) ?  (this.findOrFailID(ID)) : (this.findOrFailType(template_type)) );

      if (inst) {
        inst.push(payload);
        return inst;
      } else {
        // handle "first instance" not having an issued ID
        if (this.templateInstancePool.length === 1) {
          if (this.templateInstancePool[0]) {
            if (this.templateInstancePool[0].context.template_type === template_type) {
              if (this.templateInstancePool[0].ID === -1) {

                this.templateInstancePool[0].ID = ID;
                this.templateInstancePool[0].context.push(payload);
                return this.templateInstancePool[0].context;
              } else {
                if (this.sources[template_type]) {
                  let newInstance = this.add(this.sources[template_type], template_type, true, ID);
                  newInstance.push(payload);
                  return newInstance;
                } else {
                  //console.log('error: could not locate old template source');
                }
              }
            } else {
              //console.log('error: the only template in the pool is not the correct type');
            }
          }

          // at this stage just overwrite the fucking existing shit
        } else if (this.templateInstancePool.length > 1) {

          let findInstance = this.countInstance(template_type);
          if (findInstance.count = 1) 
          {
            let inst = findInstance.lastInstance.context;
            if (inst.template_Type === -1) {
              findInstance.lastInstance.ID = ID;
              inst.push(payload);
              return inst;
            } else {
              if (findInstance.count !== 0) {
                if (this.sources[template_type]) {


                  let newInstance = this.add(this.sources[template_type], template_type, true, ID);
                  newInstance.push(payload);
                  return newInstance;
                } else {
                  //console.log('error: could not locate old template source');
                }
              }
            }
          } 
        } 
      }
    }

    add(source, template_type, is_enabled, ID) {

      if (this.isFull()) {
        throw new Error('error: exceeds template limit (' + this.templateLimit() + ')');
      }
  
      switch (template_type) {

        
        case 1:    

          if (!this.sources[template_type]) {
            this.sources[template_type] = source;
          }

      
          let inst = new DiscussionItem(source, is_enabled);
          if (inst) {
              this.templateInstancePool.push({ID: ID, context: inst});
              return inst;
          } else {
              throw new Error('error: could not add DiscussionListItem template');
          }
          

        case 4: // welcomehero should be a single instance component

          this.sources[4] = source;
          let hinst = new WelcomeHero(source, is_enabled);
          this.templateInstancePool.push({ID: ID_SINGLE_COMPONENT, context: hinst});
          break; 


         
        
        default:
          console.log('warning: template ' + template_type + ' loaded, but is currently not supported');
        }
      }
  };
  
  export default TemplatePool;
  