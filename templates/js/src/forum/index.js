import {extend, override} from 'flarum/extend';
import {truncate} from 'flarum/utils/string';
import app from 'flarum/app';
import abbreviateNumber from 'flarum/utils/abbreviateNumber';

// Components to override
import DiscussionListItem from 'flarum/components/DiscussionListItem';
import WelcomeHero from 'flarum/components/WelcomeHero';
import DiscussionList from 'flarum/components/DiscussionList';

// Internal
import TemplatePool from './templates/TemplatePool';
import Template from '../common/models/Template';

// Globals
var templatePool = null;
const ID_SINGLE_COMPONENT = -44;
const TYPE_DISCUSSION_LISTITEM = 1;
const TYPE_WELCOME_HERO = 4;

// TODO(?): call original() before returning new view for future extension support
function distributeOverride(template_type, payload, ID = ID_SINGLE_COMPONENT) {
  if (!templatePool) 
    templatePool = new TemplatePool;

  // push the payload to the correct template instance
  let inst = templatePool.push(template_type, payload, ID);
  if (inst) return inst.view();

  // template probably isnt enabled in settings, just return original view
  return payload.props.original();
}

function welcomeHerofromProps(props, original) {
  return (
    distributeOverride (TYPE_WELCOME_HERO,
      {
        payload: {   
          foo: 'bar',          
        },
          props: {
            props: props,
            original: original
          }
      }
    )
  );   
}

function discussionComponentFromProps(instance, props, original) {

  let discussion = props.discussion;
  
    if (discussion) {

        const attrs = discussion.
          data.
          attributes;

        const firstPost = discussion.
          firstPost ();

        const user = discussion.
          user ();
        
        const avatar = user.
          avatarUrl ();

        const userURL = app.route.user(user);

        const username = user.username();
        const displayname = user.displayName();

        const excerptLength =  130;
        let excerpt = 'error: failed retrieving this post excerpt';

        if (firstPost) {
          excerpt = truncate(
            firstPost.
            contentPlain(),
            excerptLength
          );
        }


        const discussionID = discussion.data.id;
        const isUnread = discussion.isUnread();
        const showUnread = !instance.showRepliesCount() && isUnread;
        const replyCount = abbreviateNumber(discussion[showUnread ? 'unreadCount' : 'replyCount']());
      
       

      
      return (
        distributeOverride (TYPE_DISCUSSION_LISTITEM,
          {
            payload: {   
              title: attrs.title,
              username: username,
              displayName: displayname,
              userURL: userURL,
              avatarURL: (avatar) ? (avatar) : '',
              replycount: replyCount,
              discussionURL: app.route.discussion(props.discussion, props.discussion.lastPostNumber()),
              views: (attrs.views) ? (attrs.views) : 0,
              votes: (attrs.votes) ? (attrs.votes) : 0,
              answers: discussion.flagrowMasonAnswers(),
              excerpt: (excerpt) ? (excerpt) : '',
            },
              props: {
                instance: instance,
                props: props,
                original: original
              }

          }, discussionID
        )
      );     
    }}


app.initializers.add('cmc-templates', () => {
  app.store.models.templates = Template;

    extend(DiscussionList.prototype, 'requestParams', function(params) {
      console.log('Dlist');
      app.store.find('discussions', '').then(answer=> {             
       m.redraw(); // fixes MASON thread covers (probably slowing shit down?)
      });

      params.include.push('firstPost');
    });
  
    // TODO: reconsider props+original for singuar components
    override(WelcomeHero.prototype, 'view', function(original) {
      return welcomeHerofromProps(this.props, original);

    });

    override(DiscussionListItem.prototype, 'view', function(original) {
      return discussionComponentFromProps(this, this.props, original);
    });

  });
  