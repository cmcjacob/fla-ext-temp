import humanTime from 'flarum/helpers/humanTime';
import Component from 'flarum/Component';

export class AgeComponent extends Component {
  init() {
  }
  initProps() {}

  view() {
    let attrs = Object.assign({}, this.props);

    if (!this.props.discussion)  { 
      return (
        <div>no discussion
                </div>)
    }
   
  
    const lastPost = this.props.instance.showFirstPost() && this.props.discussion.replyCount();
    const age = this.props.discussion[lastPost ? 'lastPostedAt' : 'createdAt']();
    return humanTime(age);
  }
}

export default AgeComponent;
