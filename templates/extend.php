<?php

namespace CMC\Templates;

use Flarum\Extend;


use CMC\Templates\Api\Controller;
use CMC\Templates\Listeners;
use Illuminate\Contracts\Events\Dispatcher;
use Flarum\Foundation\Application;



return [
    (new Extend\Frontend('forum'))
        ->css(__DIR__ . '/less/forum.less')
        ->js(__DIR__ . '/js/dist/forum.js'),

    (new Extend\Frontend('admin'))
        ->js(__DIR__ . '/js/dist/admin.js')
        ->css(__DIR__ . '/less/admin.less'),

    (new Extend\Routes('api'))
        ->post('/templates', 'templates.create', Controller\CreateTemplateController::class)
        ->patch('/templates/{id}', 'templates.update', Controller\EditTemplateController::class)
        ->delete('/templates/{id}', 'templates.delete', Controller\DeleteTemplateController::class)
        ->post('/templates/upload', 'templates.upload', Controller\UploadTemplateController::class),

    function (Dispatcher $events, Application $app) {
        
        $events->subscribe(Listeners\TemplatesRelationships::class);
        $app->register(Providers\PackageServiceProvider::class);
        $app->register(Providers\SettingsProvider::class);
    }
       
];

