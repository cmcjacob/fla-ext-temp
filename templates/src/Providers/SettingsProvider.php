<?php
namespace CMC\Templates\Providers;
use CMC\Templates\Helpers\Settings;
use Flarum\Foundation\AbstractServiceProvider;

class SettingsProvider extends AbstractServiceProvider
{
    public function register()
    {
        $this->app->singleton(Settings::class);
    }
}