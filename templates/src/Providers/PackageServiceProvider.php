<?php


namespace CMC\Templates\Providers;

use Flarum\Foundation\AbstractServiceProvider;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Filesystem\Factory;
use League\Flysystem\FilesystemInterface;
use RuntimeException;
use CMC\Templates\Package\PackageUploader;

class PackageServiceProvider extends AbstractServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->registerPackageFilesystem();
    }
 
    protected function registerPackageFilesystem()
    {
        $packageFilesystem = function (Container $app) {
            return $app->make(Factory::class)->disk('flarum-assets')->getDriver();
        };
        $this->app->when(PackageUploader::class)
            ->needs(FilesystemInterface::class)
            ->give($packageFilesystem);
    }
    

}