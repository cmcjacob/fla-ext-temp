<?php

namespace CMC\Templates\Package;

use Flarum\Foundation\Application;
use wapmorgan\UnifiedArchive\UnifiedArchive;



class PackageDecompressor
{
    protected $archive;
    protected $settings;
    protected $requirements;
    protected $preview;
    protected $sourcepath;
    protected $sourcefiles;
    protected $sourcetype;
    protected $errors;
    protected $name;
    protected $version;
    protected $description;
    protected $author;
    protected $previewPath;
    
    // TODO: move this to some custom validator for assertion
    protected $requiredKeys = ['name', 'version', 'description'];

    public function __construct($tmppath, $filename, $publicpath, $url) 
    {
        $this->archive = UnifiedArchive::open($filename);
        if ($this->archive->isFileExists('package.json')) {
            $file_content = $this->archive->getFileContent('package.json'); // string
            if ($file_content) {
                $this->settings = json_decode($file_content, true);
                $this->parseSettings();
                if ($this->getPreviewResource()) 
                    $this->previewPath = $this->pushPreviewResource($publicpath, $url);
            }
        }
    }

    // recursively creates all nested path nodes
    private function createPath($path) {
        if (is_dir($path)) return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = $this->createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }

    private function pushPreviewResource($publicpath, $urlPath) {
        if ($this->getPreviewResource()) {
            $extract = $this->archive->extractFiles($publicpath, [$this->getPreviewResource()], true );
            if ($extract) {
                $fileName = $publicpath . '\\' . $this->getPreviewResource();
                if (file_exists($fileName)) {
                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $newPath = $publicpath . '\\assets\\templates\\' . $this->settings['name'] . '\\';
                    $newFileName = $newPath . 'p.' . $ext;
                    $this->createPath($newPath);
                    if (file_exists($newPath)) {
                        $op = rename($fileName, $newFileName);
                        if (file_exists($newFileName)) {
                            return($urlPath . '/assets/templates/' . $this->settings['name'] . '/p.' . $ext);
                        } 
                    }  
                }
        
                try {
                    unlink($fileName);
                } finally {
                    return false;
                }
            }  
        }
        return false;
    }
    public function hasErrors() {
        if ($this->settings && $this->sourcefiles && !$this->errors && $this->name && 
        $this->version)  {
            return false;
        } else {
            return($this->errors);
        }
    }

    public function getPreviewResource() {
        if ($this->preview) {
            if ($this->previewPath) {
                return $this->previewPath;
            } else {
            return $this->preview;
            }
        } 
        return false;
    }

    public function getSourceFiles() {
        if ($this->sourcefiles) {
            return $this->sourcefiles;
        } else {
            return false;
        }
    }

    public function getAttributes() {
        $res = array();
        if ($this->name)            array_push($res, ['name', $this->name ] );
        if ($this->version)         array_push($res, ['version', $this->version]);
        if ($this->requirements)    array_push($res, ['requirements', $this->requirements]);
        if ($this->description)     array_push($res, ['description', $this->description]);
        if ($this->author)          array_push($res, ['author', $this->author]);
        if ($this->previewPath)     array_push($res, ['previewPath', $this->previewPath]);
        return($res);
    }

    private function trimPath($path) {
        $p = ltrim($path, '\/');
        $p = ltrim($p, '\\');
        return $p;
    }

    private function typeFromSlug($slug) {
        switch (strtolower($slug)) {
            case "discussionlistitem":
                return 1;
                break;
            case "welcomehero":
                return 4;
                break;
            default:
                return null;
        }
    }

    private function validatePackageJSON() {
        $s = $this->settings;

        $this->sourcefiles = array();
        $this->errors = array();
        $ct = 0;

        if (isset($s['name'], $s['version'], $s['templates'])) { 

            foreach($s['templates'] as $key => $item) {

                $template_type = $this->typeFromSlug($key);

                switch ($template_type) {
                    case "1":
                    case "4":
                        if (isset($item['sourcefile'])) {
                            
                            $s = $this->trimPath($item['sourcefile']);
                            if ($this->archive->isFileExists($s)) {
                                $src = $this->archive->getFileContent($s);
                                array_push($this->sourcefiles, [$template_type, $src, (isset($item['enabled']) ? (bool)$item['enabled'] : false)] );
                            } else {
                                array_push($this->errors, 'Cannot find sourcefile ' . $item['sourcefile'] . ' in "discussionlistitem" template');
                            }
                        } else {
                            array_push($this->errors, 'No "sourcefile" key was found for template "discussionlistitem" in package.json.  A template in a package must point to an existing sourcefile in the archive.');
                        }
                        break;        
                        
                    case null:
                        array_push($this->errors, 'NULL template type (fatal): "' . $key . '" is not a recognized template.  Check documentation for valid types.');
                        break;

                    default:
                        array_push($this->errors, 'Unresolved template type: "' . $key . '" is not a recognized template.  Check documentation for valid types.');
                        break;                    
                }
            }
            
            $s = $this->settings;

            
            $this->name = $s['name'];
            $this->version = $s['version'];

            if (isset($s['description'])) {
                $this->description = $s['description'];
            }
            if (isset($s['requirements'])) {
                $this->requirements = $s['requirements'];
            }
            if (isset($s['preview'])) {
                $p = $this->trimPath($s['preview']);
                if ($this->archive->isFileExists($p)) {
                    $this->preview = $p;
                } else {
                    array_push($this->errors, 'Cannot resolve resource "' . $s['preview'] . '" in archive. Check the path and make sure the file exists.');
                }

                $this->description = $s['description'];
            }

            
        } else {
            array_push($this->errors, 'Could not find all required keys: name, version, and templates must all exist in package.json.  Check your archive package settings.');
        }
        
        return $this->errors;       
     }

    public function parseSettings($settings=null) 
    {
        if (!$settings) {
            if ($this->settings) 
                $this->validatePackageJSON();
        }
        return $this->settings;
    }

}