<?php 

namespace CMC\Templates\Package;

use Illuminate\Support\Str;
use Intervention\Image\Image;
use League\Flysystem\FilesystemInterface;
use Flarum\User\User;

class PackageUploader
{
    protected $uploadDir;
    public function __construct(FilesystemInterface $uploadDir)
    {
      
        $this->uploadDir = $uploadDir;
      
    }
    /**
     * @param User $user
     * @param Image $image
     */
    public function upload()
    {
        /*
        if (extension_loaded('exif')) {
            $image->orientate();
        }
        $encodedImage = $image->fit(100, 100)->encode('png');*/
       // $avatarPath = Str::random().'.tar.gz';
        //$this->remove($user);
        //$user->changeAvatarPath($avatarPath);
       // $this->uploadDir->put($avatarPath, $image);
    }
    public function remove(User $user)
    {
        $avatarPath = $user->avatar_path;
        $user->afterSave(function () use ($avatarPath) {
            if ($this->uploadDir->has($avatarPath)) {
                $this->uploadDir->delete($avatarPath);
            }
        });
        $user->changeAvatarPath(null);
    }
}