<?php

namespace CMC\Templates\Validators;

use Flarum\Foundation\AbstractValidator;

class PackageValidator extends AbstractValidator
{
    protected $rules = [
        'templatepackage' => [
            'required',
            'mimes:tar,tar.gz,zip,rar',
            'max:20000'
        ]
    ];
}