<?php

namespace CMC\Templates\Validators;

use Flarum\Foundation\AbstractValidator;

class TemplateValidator extends AbstractValidator
{
    protected $rules = [
        'source' => ['required'],
        'template_type' => ['unique:templates'],
        'is_enabled' => ['required'],

    
    
    ];
}