<?php

namespace CMC\Templates\Command;

use Flarum\Foundation\Application;
use Flarum\Foundation\DispatchEventsTrait;
use Flarum\User\AssertPermissionTrait;
use Flarum\User\User;
use CMC\Templates\Package\PackageUploader;
use CMC\Templates\Package\PackageDecompressor;
use CMC\Templates\Validators\PackageValidator;
use CMC\Templates\Api\Controller\TemplateRepository;
use Illuminate\Contracts\Events\Dispatcher;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use RuntimeException;
use CMC\Templates\Api\Controller\Template;

class UploadPackageHandler
{
    use DispatchEventsTrait;
    use AssertPermissionTrait;
    /**
     * @var \Flarum\User\UserRepository
     */
    protected $templates;
    protected $events;
    /**
     * @var Application
     */
    protected $app;
    /**
     * @var CMC\Templates\Uploader\PackageUploader
     */
    protected $uploader;
    /**
     * @var \CMC\Templates\Validators\PackageValidator
     */
    protected $validator;
    /**
     * @param Dispatcher $events
     * @param UserRepository $users
     * @param Application $app
     * @param PackageUploader $uploader
     * @param PackageValidator $validator
     */
    public function __construct(Dispatcher $events, TemplateRepository $templates, Application $app, PackageValidator $validator)
    {
     
        $this->events = $events;
        $this->templates = $templates;
        $this->app = $app;
        $this->validator = $validator;
      
    }
    /**
     * @param UploadPackage $command
     * @return \Flarum\User\User
     * @throws \Flarum\User\Exception\PermissionDeniedException
     */
    public function handle(UploadPackage $command)
    {
        $actor = $command->actor;

        
        //$this->assertAdmin($actor);
        
        $file = $command->file;
        $tmpFile = tempnam($this->app->storagePath().'/tmp', 'templatepackage');
        $file->moveTo($tmpFile);


        try {
            $file = new UploadedFile(
                $tmpFile,
                $file->getClientFilename(),
                $file->getClientMediaType(),
                $file->getSize(),
                $file->getError(),
                true
            );
            $this->validator->assertValid(['templatepackage' => $file]);

            $extractArchive = new PackageDecompressor($this->app->storagePath() . '/tmp', $tmpFile, $this->app->publicPath(), $this->app->url());

            if ($extractArchive) {
                $err = $extractArchive->hasErrors();
                if ($err) {
                    var_dump($err);
                   // throw new RuntimeException("Errors encountered while parsing template archive: $e", $err);
                } else {
                    $attrs = $extractArchive->getAttributes();
                    if ($attrs) {
                        $tmp = new Template();

                        foreach($attrs as $attr) {

                            switch ($attr[0]) {
                                case "name":
                                    $tmp->name = $attr[1];
                                    break;
                                case "author":
                                    $tmp->author = $attr[1];
                                    break;
                                case "description":
                                    $tmp->description = $attr[1];
                                    break;
                                case "version":
                                    $tmp->version = $attr[1];
                                    break;
                                case "requirements":
                                    $tmp->requirements = $attr[1];
                                    break;
                                case "previewPath":
                                    $tmp->previewPath = $attr[1];
                                    break;
                                  
    
                            }
                        }
                     
                        // TODO: Support multiple templates per archive, and not only the first one.
                        $tmp->template_type = $extractArchive   ->getSourceFiles()[0][0];
                        $tmp->source = $extractArchive          ->getSourceFiles()[0][1];
                        $tmp->is_enabled = $extractArchive      ->getSourceFiles()[0][2];

                        $this->validator->assertValid($tmp->getDirty());
                        $tmp->save();
                    }
                }
            }

        } finally {
           @unlink($tmpFile);
        }
        return $command;
    }
}