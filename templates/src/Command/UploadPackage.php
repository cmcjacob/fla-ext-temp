<?php

namespace CMC\Templates\Command;

use Flarum\User\User;
use Psr\Http\Message\UploadedFileInterface;

class UploadPackage
{
    /**
     * The avatar file to upload.
     *
     * @var UploadedFileInterface
     */
    public $file;
    /**
     * The user performing the action.
     *
     * @var User
     */
    public $actor;
    /**
     * @param int $userId The ID of the user to upload the avatar for.
     * @param UploadedFileInterface $file The avatar file to upload.
     * @param User $actor The user performing the action.
     */
    public function __construct(UploadedFileInterface $file)
    {
        
        $this->file = $file;

    }
}