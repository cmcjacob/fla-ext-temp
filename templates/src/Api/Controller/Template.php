<?php

namespace CMC\Templates\Api\Controller;

use Flarum\Database\AbstractModel;

class Template extends AbstractModel
{
    protected $table = 'templates';

    protected $casts = [
        'id' => 'integer',
        'template_type' => 'integer',
        'is_enabled' => 'boolean',
        'requirements' => 'json',
        'source' => 'longtext',
        'version' => 'text',
        'description' => 'text',
        'author' => 'text',
        'previewPath' => 'text'
    ];

    public static function build($template_type, $source, $is_enabled, $name, $version="1.0", $requirements='', $description='', $author='', $previewPath='')
    {    
        $template = new static;
        $template->template_type               = $template_type;
        $template->source             = $source;
        $template->is_enabled         = (bool) $is_enabled;
        $template->name = $name;
        $template->version = $version;
        $template->requirements = $requirements;
        $template->description = $description;
        $template->author = $author;
        $template->previewPath = $previewPath;
        return $template;
    }
}