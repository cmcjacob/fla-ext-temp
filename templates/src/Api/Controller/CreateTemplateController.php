<?php

namespace CMC\Templates\Api\Controller;
use Flarum\Api\Controller\AbstractCreateController;
use CMC\Templates\Api\Serializer\TemplateSerializer;
use Illuminate\Contracts\Bus\Dispatcher;
use Psr\Http\Message\ServerRequestInterface;
use Tobscure\JsonApi\Document;

class CreateTemplateController extends AbstractCreateController
{
    /**
     * @inheritdoc
     */
    public $serializer = TemplateSerializer::class;
    /**
     * @var Dispatcher
     */
    protected $bus;
    /**
     * @param Dispatcher $bus
     */
    public function __construct(Dispatcher $bus)
    {
        $this->bus = $bus;
    }
    /**
     * {@inheritdoc}
     */
    protected function data(ServerRequestInterface $request, Document $document)
    {
        $doesExist = array_get($request->getParsedBody(), 'data');

        return $this->bus->dispatch(
            new CreateTemplate($request->getAttribute('actor'), array_get($request->getParsedBody(), 'data'))
        );
    }
}