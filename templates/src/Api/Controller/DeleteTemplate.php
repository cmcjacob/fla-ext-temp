<?php

namespace CMC\Templates\Api\Controller;
use Flarum\User\User;

class DeleteTemplate
{
    /**
     * The ID of the template to delete.
     *
     * @var int
     */
    public $templateID;
    /**
     * The user performing the action.
     *
     * @var User
     */
    public $actor;
    /**
     * Any other link input associated with the action. This is unused by
     * default, but may be used by extensions.
     *
     * @var array
     */
    public $data;
    /**
     * @param int $linkId The ID of the link to delete.
     * @param User $actor The user performing the action.
     * @param array $data Any other link input associated with the action. This
     *     is unused by default, but may be used by extensions.
     */
    public function __construct($templateID, User $actor, array $data = [])
    {
      
        $this->templateID = $templateID;
        $this->actor = $actor;
        $this->data = $data;
    }
}