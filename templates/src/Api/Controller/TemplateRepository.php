<?php

namespace CMC\Templates\Api\Controller;

use Flarum\User\User;
use Illuminate\Database\Eloquent\Builder;
use Flarum\Foundation\Application;
use CMC\Templates\Contracts\UploadAdapter;

class TemplateRepository
{

  protected $path;
  /**
   * @var UploadValidator
   */

  public function __construct(Application $app)
  {
      $this->path = $app->storagePath();
  }

  public function findOrFail($id, User $actor = null) {


   
    return Template::where('id', $id)->firstOrFail();
  }

  protected function query()
  {
      return $this->field->newQuery()->orderBy('sort', 'desc');
  }



}