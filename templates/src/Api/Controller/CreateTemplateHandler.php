<?php

namespace CMC\Templates\Api\Controller;
use Flarum\User\AssertPermissionTrait;
use CMC\Templates\TemplateValidator;

class CreateTemplateHandler
{
    use AssertPermissionTrait;
    /**
     * @var LinkValidator
     */
    protected $validator;
    /**
     * @param LinkValidator $validator
     */
    public function __construct(TemplateValidator $validator)
    {
        $this->validator = $validator;
    }
    /**
     * @param CreateLink $command
     * @return Link
     */
    public function handle(CreateTemplate $command)
    {
        $actor = $command->actor;
        $data = $command->data;
        $this->assertAdmin($actor);
        $template = Template::build(
            array_get($data, 'attributes.template_type'),
            array_get($data, 'attributes.source'),
            array_get($data, 'attributes.is_enabled'),
        );
        $this->validator->assertValid($template->getAttributes());
        $template->save();
        return $template;
    }
}