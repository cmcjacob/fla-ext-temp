<?php
/*
 * (c) Sajjad Hashemian <wolaws@gmail.com>
 */
namespace CMC\Templates\Api\Controller;
use Flarum\User\AssertPermissionTrait;
use CMC\Templates\Validators\TemplateValidator;

class EditTemplateHandler
{

    use AssertPermissionTrait;
    /**
     * @var LinkRepository
     */
    protected $templates;
    /**
     * @var LinkValidator
     */
    protected $validator;
    /**
     * @param LinkRepository $links
     * @param LinkValidator $validator
     */
    public function __construct(TemplateRepository $templates, TemplateValidator $validator)
    {
        $this->templates = $templates;
        $this->validator = $validator;
    }
    /**
     * @param EditLink $command
     * @return \Sijad\Links\Link
     * @throws \Flarum\User\Exception\PermissionDeniedException
     */
    public function handle(EditTemplate $command)
    {
     
      
        $actor = $command->actor;
        $data = $command->data;
        $template = $this->templates->findOrFail($command->templateID, $actor);
        $this->assertAdmin($actor);
        $attributes = array_get($data, 'attributes', []);
        if (isset($attributes['template_type'])) {
            $template->template_type = $attributes['template_type'];
        }
        if (isset($attributes['source'])) {
            $template->source = $attributes['source'];
        }
        if (isset($attributes['is_enabled'])) {
            $template->is_enabled = $attributes['is_enabled'];
        }
        if (isset($attributes['isEnabled'])) {
            $template->is_enabled = $attributes['is_enabled'];
        }
        if (isset($attributes['requirements'])) {
            $template->is_enabled = $attributes['requirements'];
        }
        if (isset($attributes['description'])) {
            $template->is_enabled = $attributes['description'];
        }
        if (isset($attributes['author'])) {
            $template->is_enabled = $attributes['author'];
        }
        if (isset($attributes['previewPath'])) {
            $template->is_enabled = $attributes['previewPath'];
        }


      

        $this->validator->assertValid($template->getDirty());
        $template->save();
        return $template;
    }
}