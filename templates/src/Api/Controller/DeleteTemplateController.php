<?php

namespace CMC\Templates\Api\Controller;

use Flarum\Api\Controller\AbstractDeleteController;

use Illuminate\Contracts\Bus\Dispatcher;

use Flarum\User\AssertPermissionTrait;
use Illuminate\Support\Arr;
use Psr\Http\Message\ServerRequestInterface;


error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");

class DeleteTemplateController extends AbstractDeleteController
{
    /**
     * @var Dispatcher
     */
    use AssertPermissionTrait;

    protected $bus;

    /**
     * @param Dispatcher $bus
     */
    public function __construct(Dispatcher $bus)
    {
    
        $this->bus = $bus;
    }
    /**
     * {@inheritdoc}
     */
    protected function delete(ServerRequestInterface $request)
    {
       
        $this->assertAdmin($request->getAttribute('actor'));

        $this->bus->dispatch(
            new DeleteTemplate(array_get($request->getQueryParams(), 'id'), $request->getAttribute('actor'))
        );
        
    }
}
