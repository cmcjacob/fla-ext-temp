<?php

namespace CMC\Templates\Api\Controller;

use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Flarum\User\AssertPermissionTrait;
use Flarum\User\User;
use CMC\Templates\Command\UploadPackage;
use Zend\Diactoros\Response;

use Zend\Diactoros\Response\HtmlResponse;


class UploadTemplateController implements RequestHandlerInterface {

    use AssertPermissionTrait;

    /**
     * @var Dispatcher
     */
    protected $bus;
    public function __construct( Dispatcher $bus)
    {
        $this->bus = $bus;

    }
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @throws \Flagrow\Upload\Exceptions\InvalidUploadException
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $actor = $request->getAttribute('actor');
        $this->assertAdmin($request->getAttribute('actor'));
        $file = Arr::get($request->getUploadedFiles(), 'template');
        $collection = $this->bus->dispatch(
            new UploadPackage($file, $actor)
        ); 

       
        return new Response('Upload Complete', 200);
        
    }
}
