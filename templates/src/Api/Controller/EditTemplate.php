<?php
/*
 * (c) Sajjad Hashemian <wolaws@gmail.com>
 */
namespace CMC\Templates\Api\Controller;
 use Flarum\User\User;

class EditTemplate
{
    /**
     * The ID of the link to edit.
     *
     * @var int
     */
    public $templateID;
    /**
     * The user performing the action.
     *
     * @var User
     */
    public $actor;
    /**
     * The attributes to update on the link.
     *
     * @var array
     */
    public $data;
    /**
     * @param int $linkId The ID of the link to edit.
     * @param User $actor The user performing the action.
     * @param array $data The attributes to update on the link.
     */
    public function __construct($templateID, User $actor, array $data)
    {
        $this->templateID = $templateID;
        $this->actor = $actor;
        $this->data = $data;
    }
}