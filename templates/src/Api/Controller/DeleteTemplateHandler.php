<?php

namespace CMC\Templates\Api\Controller;
use Flarum\User\AssertPermissionTrait;
use CMC\Templates\Api\Controller;

class DeleteTemplateHandler
{
    use AssertPermissionTrait;
    /**
     * @var LinkRepository
     */
    protected $templates;
    /**
     * @param LinkRepository $links
     */
    public function __construct(TemplateRepository $templates)
    {
        $this->templates = $templates;
    }
    /**
     * @param DeleteLink $command
     * @return \Sijad\Links\Link
     * @throws \Flarum\User\Exception\PermissionDeniedException
     */
    public function handle(DeleteTemplate $command)
    {
        $actor = $command->actor;
        $template = $this->templates->findOrFail($command->templateID, $actor);
        $this->assertAdmin($actor);
        $template->delete();
        return $template;
    }
}