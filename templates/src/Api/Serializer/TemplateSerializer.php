<?php

namespace CMC\Templates\Api\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;
use Flarum\Api\Serializer\DiscussionSerializer;

class TemplateSerializer extends AbstractSerializer
{

    protected $type = 'templates';

    protected function getDefaultAttributes($template)
    {
        $attributes = [
            'id'        => $template->id,
            'template_type'      => $template->template_type,
            'source'    => $template->source,
            'is_enabled' => $template->is_enabled,
            'requirements' => $template->requirements,
            'description' => $template->description,
            'author' => $template->author,
            'previewPath' => $template->previewPath,
            'name' => $template->name,
        ];
        return $attributes;
    }
}